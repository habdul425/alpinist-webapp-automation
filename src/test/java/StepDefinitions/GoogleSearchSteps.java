package StepDefinitions;

import java.net.ProxySelector;
import java.util.concurrent.TimeUnit;
import junit.runner.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.model.Log;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;


public class GoogleSearchSteps {

	
	static WebDriver driver = null;
	@SuppressWarnings("deprecation")

	@Given("browser is open")
	public void browser_is_open() {
		
		
		System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("enable-automation");
		options.addArguments("--headless");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-extensions");
		options.addArguments("--dns-prefetch-disable");
		options.addArguments("--disable-gpu");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		
		driver = new ChromeDriver(options);
		driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/register");
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
	}
	/*@Given("Navigate to Alpinist URL")
	public void navigate_to_alpinist_url() throws InterruptedException {
		driver.navigate().to("https://devpro1-alpinistcrm.cs173.force.com/#/register");
		//Thread.sleep(2000);
	}*/
	@Then("Enter the firstname for Register Scenario1")
	public void enter_the_firstname_for_register_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the firstname for Register Scenario1");
		
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rameez");
		Thread.sleep(2000);
	}
	@Then("Enter the lastname for Register Scenario1")
	public void enter_the_lastname_for_register_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the lastname for Register Scenario1");
		
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Hameed");	
		Thread.sleep(2000);
	}
	@Then("Enter the email field for Register Scenario1")
	public void enter_the_email_field_for_register_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the email field for Register Scenario1");
		driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("alpinist1264@gmail.com");
		Thread.sleep(2000);
	}
	@Then("Choose the DOB for Register Scenario1")
	public void choose_the_dob_for_register_scenario1() {
		driver.findElement(By.xpath("//div[@class='single-date-picker flex align-center']")).click();
	}
	@Then("Choose the DOBYear for Register Scenario1")
	public void choose_the_dob_year_for_register_scenario1() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
	}
	@Then("Choose the DOBMonth for Register Scenario1")
	public void choose_the_dob_month_for_register_scenario1() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
	}
	@Then("Choose the DOBDate for Register Scenario1")
	public void choose_the_dob_date_for_register_scenario1() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
	}
	@Then("click the Confirm Date button for Register Scenario1")
	public void click_the_confirm_date_button_for_register_scenario1() {
		driver.findElement(By.xpath("//span[contains(text(),'Bestätigen')]")).click();
	}
	@Then("Enter and enter the password for Register Scenario1")
	public void enter_and_enter_the_password_for_register_scenario1() {
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
	}
	@Then("Enter and enter the confirm password for Register Scenario1")
	public void enter_and_enter_the_confirm_password_for_register_scenario1() {
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
	}
	@Then("click the Continue button for Register Scenario1")
	public void click_the_continue_button_for_register_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[contains(text(),'Weiter')]")).click();
		Thread.sleep(2000);
	}




	/*@Then("Click the first checkboxes for Register Screen2 Scenario1")
	public void click_the_first_checkboxes_for_register_screen2_scenario1() {
		System.out.println("Inside step - Click the first checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[1]")).click();
	}

	@Then("Click the Second checkboxes for Register Screen2 Scenario1")
	public void click_the_second_checkboxes_for_register_screen2_scenario1() {
		System.out.println("Inside step - Click the Second checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[2]")).click();
	}

	@Then("Click the third checkboxes for Register Screen2 Scenario1")
	public void click_the_third_checkboxes_for_register_screen2_scenario1() {
		System.out.println("Inside step - Click the third checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[3]")).click();
	}*/

	@Then("click the Next button for Register Screen2 Scenario1")
	public void click_the_next_button_for_register_screen2_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Next button for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
	}

	@Then("click first location for Register Screen3 Scenario1")
	public void click_first_location_for_register_screen3_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click any location for Register Screen3 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[1]")).click();
		Thread.sleep(2000);
	}

	@Then("click the Next button for Register Screen3 Scenario1")
	public void click_the_next_button_for_register_screen3_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Next button for Register Screen3 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
	}

	@Then("click the gender for Register Screen4 Scenario1")
	public void click_the_gender_for_register_screen4_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the gender for Register Screen4 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
		Thread.sleep(2000);
	}
	

	@Then("Slide the Height for Register Screen4 Scenario1")
	public void slide_the_height_for_register_screen4_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Height for Register Screen4 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i <= 90 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
	}

	@Then("Slide the Weight for Register Screen4 Scenario1")
	public void slide_the_weight_for_register_screen4_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Weight for Register Screen4 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i <= 50 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
	}

	@Then("Slide the Shoe Size for Register Screen4 Scenario1")
	public void slide_the_shoe_size_for_register_screen4_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Shoe Size for Register Screen4 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 24; i <= 30 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);
	}
	
	/*@Then("Enter the Ski boots size for Register Screen4 Scenario1")
	public void Enter_the_Ski_boots_size_for_Register_Screen4_Scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the Ski boots size for Register Screen4 Scenario1");
		
		driver.findElement(By.xpath("//input[@type='number']")).sendKeys("");
		
		Thread.sleep(2000);
	}*/

	@Then("click the Continue button for Register Screen4 Scenario1")
	public void click_the_continue_button_for_register_screen4_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Continue button for Register Screen4 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		
		Thread.sleep(2000);
	}

	@Then("click the Skill type for Register Screen5 Scenario1")
	public void click_the_skill_type_for_register_screen5_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Skill type for Register Screen5 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
		Thread.sleep(2000);
		}

	@Then("Slide the Select Skill for Register Screen5 Scenario1")
	public void slide_the_select_skill_for_register_screen5_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Select Skill for Register Screen5 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);
	}

	@Then("Slide the Select Gear grade for Register Screen5 Scenario1")
	public void slide_the_select_gear_grade_for_register_screen5_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Select Gear grade for Register Screen5 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);
	}

	@Then("click the Continue button for Register Screen5 Scenario1")
	public void click_the_continue_button_for_register_screen5_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Continue button for Register Screen5 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
		}

	@Then("click the Transport type for Register Screen6 Scenario1")
	public void click_the_transport_type_for_register_screen6_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Transport type for Register Screen6 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[1]")).click();
		Thread.sleep(2000);
		}
	
	@Then("Enter the number plate value for Register Screen6 Scenario1")
	public void Enter_the_number_plate_value_for_Register_Screen6_Scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the number plate value for Register Screen6 Scenario1");
		
		driver.findElement(By.xpath("//input[@name='license']")).sendKeys("TN38AA");
		Thread.sleep(2000);
	}

	@Then("click the Next button for Register Screen6 Scenario1")
	public void click_the_next_button_for_register_screen6_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Next button for Register Screen6 Scenario1");
		
		driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
	}

	@Then("Slide the Accomodation for Register Screen7 Scenario1")
	public void slide_the_accomodation_for_register_screen7_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Slide the Accomodation for Register Screen7 Scenario1");
		
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
	}

	@Then("click the Next button for Register Screen7 Scenario1")
	public void click_the_next_button_for_register_screen7_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Next button for Register Screen7 Scenario1");
		
		driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
	}

	@Then("Enter the Street for Register Screen8 Scenario1")
	public void enter_the_street_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the Street for Register Screen8 Scenario1");
		
		driver.findElement(By.xpath("//input[@id='street']")).sendKeys("Pammal");
		Thread.sleep(2000);
	}

	@Then("Enter the Number for Register Screen8 Scenario1")
	public void enter_the_number_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the Number for Register Screen8 Scenario1");
		
		driver.findElement(By.xpath("//input[@id='number']")).sendKeys("135");
		Thread.sleep(2000);
	}

	@Then("Enter the Zip code for Register Screen8 Scenario1")
	public void enter_the_zip_code_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the Zip code for Register Screen8 Scenario1");
		
		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("640007");
		Thread.sleep(2000);
	}

	@Then("Enter the City for Register Screen8 Scenario1")
	public void enter_the_city_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Enter the City for Register Screen8 Scenario1");
		
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Chennai");
		Thread.sleep(2000);
	}

	@Then("Choose the Country for Register Screen8 Scenario1")
	public void choose_the_country_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Choose the Country for Register Screen8 Scenario1");
		
		Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='countrySelect']")));
		drpCountry.selectByVisibleText("🇮🇳 Indien");
		Thread.sleep(2000);
	}

	@Then("click the Continue button for Register Screen8 Scenario1")
	public void click_the_continue_button_for_register_screen8_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Continue button for Register Screen8 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
	}

	
	@Then("Click the next button for Register Screen9 Scenario1")
	public void Click_the_next_button_for_Register_Screen9_Scenario1() throws InterruptedException {
		//System.out.println("Inside step - Click the next button for Register Screen9 Scenario1");
		
		driver.findElement(By.xpath("//button[contains(text(),' Weiter ')]")).click();
		Thread.sleep(2000);
	}

	@Then("Click the first checkboxes for Register Screen10 Scenario1")
	public void click_the_first_checkboxes_for_register_screen10_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Click the first checkboxes for Register Screen10 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
		Thread.sleep(2000);
	}

	@Then("Click the second checkboxes for Register Screen10 Scenario1")
	public void click_the_second_checkboxes_for_register_screen10_scenario1() throws InterruptedException {
		//System.out.println("Inside step - Click the second checkboxes for Register Screen10 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
		Thread.sleep(2000);
	}

	@Then("click the Complete Sign Up button for Register Screen10 Scenario1")
	public void click_the_complete_sign_up_button_for_register_screen10_scenario1() throws InterruptedException {
	    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Anmeldung abschliessen']")).click();
		Thread.sleep(2000);
	
		
	}
	
	/*@Then("Click the checkboxes for Register Screen11 Scenario1")
	public void click_the_checkboxes_for_register_screen11_scenario1() {
		System.out.println("Inside step - Click the checkboxes for Register Screen11 Scenario1");
		
		driver.findElement(By.xpath("//div[@class='checkbox-text']")).click();
	}*/
	
	@Then("Click the promocode checkbox for Register Screen11 Scenario1")
	public void Click_the_promocode_checkbox_for_Register_Screen11_Scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Continue button for Register Screen8 Scenario1");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Ich habe einen Promo Code']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);

	}
	
	@Then("Click promocode for Register Screen11 Scenario1")
	public void Click_promocode_for_Register_Screen11_Scenario1() throws InterruptedException {
	    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='promo']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	
	@Then("Enter promocode for Register Screen11 Scenario1")
	public void Enter_promocode_for_Register_Screen11_Scenario1() throws InterruptedException {
	    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
		
		driver.findElement(By.xpath("//input[@name='promo']")).sendKeys("1111122222");
		Thread.sleep(2000);
	}
	
	@Then("click the Continue button for Register Screen11 Scenario1")
	public void click_the_continue_button_for_register_screen11_scenario1() throws InterruptedException {
		//System.out.println("Inside step - click the Continue button for Register Screen11 Scenario1");
		
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(3000);
	}
	@Then("Click Menu button for menu screen Scenario1")
	public void click_menu_button_for_menu_screen_scenario1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='header-btns hamburger-style']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
		//driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
		//Thread.sleep(2000);
	}
	@Then("Click My Profile for menu screen Scenario1")
	public void click_my_profile_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Mein Profil']")).click();
		Thread.sleep(2000);
	}
	@Then("Click My Athletic Profile for menu screen Scenario1")
	public void click_my_athletic_profile_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Mein Körperprofil']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the back arrow for menu screen Scenario1")
	public void click_the_back_arrow_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='header-btns']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the My Kids for menu screen Scenario1")
	public void click_the_my_kids_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Meine Kinder']")).click();
		Thread.sleep(3000);
	}
	@Then("Click  Add more kids button for menu screen Scenario1")
	public void click_add_more_kids_button_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='addkids-button']")).click();
		Thread.sleep(3000);
	}
	@Then("Enter the firstname for menu screen Scenario1")
	public void enter_the_firstname_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Hameed");
		Thread.sleep(3000);
	}
	@Then("Enter the lastname for menu screen Scenario1")
	public void enter_the_lastname_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Rameez");
		Thread.sleep(3000);
	}
	@Then("Choose the DOB for menu screen Scenario1")
	public void choose_the_dob_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//img[@height='16']")).click();
		Thread.sleep(3000);
	}
	@Then("Choose the DOBYear for menu screen Scenario1")
	public void choose_the_dob_year_for_menu_screen_scenario1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]")).click();
		Thread.sleep(3000);
	}
	@Then("Choose the DOBMonth for menu screen Scenario1")
	public void choose_the_dob_month_for_menu_screen_scenario1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
		Thread.sleep(3000);
	}
	@Then("Choose the DOBDate for menu screen Scenario1")
	public void choose_the_dob_date_for_menu_screen_scenario1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[25]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
		Thread.sleep(3000);
	}
	@Then("click the Confirm Date button for menu screen Scenario1")
	public void click_the_confirm_date_button_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='bestätigen']")).click();
		Thread.sleep(3000);
	}
	@Then("click the gender for menu screen Scenario1")
	public void click_the_gender_for_menu_screen_scenario1() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Männlich']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//p[text()='Male']")).click();
		Thread.sleep(3000);
	}
	@Then("Slide the Height for menu screen Scenario1")
	public void slide_the_height_for_menu_screen_scenario1() throws InterruptedException {
		WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i < 80 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
		//Thread.sleep(3000);
	}
	@Then("Slide the Weight for menu screen Scenario1")
	public void slide_the_weight_for_menu_screen_scenario1() throws InterruptedException {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i < 30 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
		//Thread.sleep(3000);
	}
	@Then("Slide the Shoe Size for menu screen Scenario1")
	public void slide_the_shoe_size_for_menu_screen_scenario1() throws InterruptedException {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 12; i <= 20 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
		//Thread.sleep(3000);
	}
	@Then("Click Add Kids for menu screen Scenario1")
	public void click_add_kids_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()=' Kinder hinzufügen']")).click();
		Thread.sleep(3000);
	}
	@Then("click the back arrow add kids for menu screen Scenario1")
	public void click_the_back_arrow_add_kids_for_menu_screen_Scenario1() throws InterruptedException{
		driver.findElement(By.xpath("//img[@width='24']")).click();
		Thread.sleep(2000);
	}
	@Then("Click Transportation Preferences for menu screen Scenario1")
	public void click_transportation_preferences_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Bevorzugte Transportmittel']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the back arrow transportation for menu screen Scenario1")
	public void click_the_back_arrow_transportation_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
	}
	@Then("Click Accommodation Preferences for menu screen Scenario1")
	public void click_accommodation_preferences_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Bevorzugte Unterkunft']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the back arrow accommodation for menu screen Scenario1")
	public void click_the_back_arrow_accommodation_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
	}
	@Then("Click Preferred Locations for menu screen Scenario1")
	public void click_preferred_locations_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Bevorzugte Skigebiete']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the back arrow location for menu screen Scenario1")
	public void click_the_back_arrow_location_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
	}
	@Then("Click Communication Preferences for menu screen Scenario1")
	public void click_communication_preferences_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Bevorzugte Kommunikationsart']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the back arrow communication for menu screen Scenario1")
	public void click_the_back_arrow_communication_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
	}
	@Then("Click the Menu to go Home for booking for menu screen Scenario1")
	public void click_the_menu_to_go_home_for_booking_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
		Thread.sleep(3000);
	}
	@Then("click the Home button for menu screen Scenario1")
	public void click_the_home_button_for_menu_screen_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Home']")).click();
		Thread.sleep(3000);
	}
	@Then("url")
	public void url() {
		
		
		System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		
		//options.addArguments("headless");
		//options.addArguments("disable-gpu");
		
		driver = new ChromeDriver(options);
		driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
	}
	
	@Then("Click Plan a trip button for Screen12 Scenario1")
	public void click_plan_a_trip_button_for_screen12_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
		Thread.sleep(2000);
	}
	
	@Then("Select first location checkbox for Screen13 Scenario1")
	public void select_first_location_checkbox_for_screen13_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Braunwald']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen13 Scenario1")
	public void click_next_button_for_screen13_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
	}

	@Then("Click on start date button for Screen14 Scenario1")
	public void click_on_start_date_button_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Select start date for Screen14 Scenario1")
	public void select_start_date_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='16. November 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("select End date for Screen14 Scenario1")
	public void select_end_date_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='19. November 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("Click start time dropdown for Screen14 Scenario1")
	public void click_start_time_dropdown_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
		Thread.sleep(2000);
		
	}
	@Then("select start time for Screen14 Scenario1")
	public void select_start_time_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Click End time dropdown for Screen14 Scenario1")
	public void click_end_time_dropdown_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("select end timefor Screen14 Scenario1")
	public void select_end_timefor_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("Click confirm button for Screen14 Scenario1")
	public void click_confirm_button_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Bestätigen']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen14 Scenario1")
	public void click_next_button_for_screen14_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
	}

	@Then("Select first equipment for Screen15 Scenario1")
	public void select_first_equipment_for_screen15_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//div[text()='Ski']")).click();
		Thread.sleep(2000);
	}
	@Then("Select second equipment for Screen15 Scenario1")
	public void select_second_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Skischuhe']")).click();
	}
	@Then("Select third equipment for Screen15 Scenario1")
	public void select_third_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Skijacke']")).click();
	}
	@Then("Select fourth equipment for Screen15 Scenario1")
	public void select_fourth_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Skihose']")).click();
	}
	@Then("Select fifth equipment for Screen15 Scenario1")
	public void select_fifth_equipment_for_screen15_scenario1() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Skistöcke']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Ski Poles']")).click();
	}
	@Then("Select sixth equipment for Screen15 Scenario1")
	public void select_sixth_equipment_for_screen15_scenario1() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Helm']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Helmet']")).click();
	}
	@Then("Select seventh equipment for Screen15 Scenario1")
	public void select_seventh_equipment_for_screen15_scenario1() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
	}
	@Then("Select eighth equipment for Screen15 Scenario1")
	public void select_eighth_equipment_for_screen15_scenario1() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboardschuhe']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
	}
	@Then("Click Yes button for Screen15 Scenario1")
	public void click_yes_button_for_screen15_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()=' Ja, Buchung hinzufügen ']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen16 Scenario1")
	public void click_next_button_for_screen16_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Weiter']")).click();
		Thread.sleep(2000);
	}
	
	@Then("Click checkbox for Screen17 Scenario1")
	public void click_checkbox_for_screen17_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//p[contains(text(),'Ich möchte nach Buchungsabschluss')]")).click();
		Thread.sleep(2000);
	}

	@Then("Click yes button for Screen17 Scenario1")
	public void click_yes_button_for_screen17_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
	}

	@Then("Click Payment button for Screen18 Scenario1")
	public void click_payment_button_for_screen18_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Weiter zur Bezahlung']")).click();
		Thread.sleep(2000);
	}
	@Then("Enter card number for Screen19 Scenario1")
	public void enter_card_number_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='cardNumber']")).sendKeys("4242 4242 4242 4242");
		Thread.sleep(2000);
	}
	@Then("Enter expiry for Screen19 Scenario1")
	public void enter_expiry_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='cardExpiry']")).sendKeys("0624");
		Thread.sleep(2000);
	}
	@Then("Enter ccv number for Screen19 Scenario1")
	public void enter_ccv_number_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='cardCvc']")).sendKeys("987");
		Thread.sleep(2000);
	}
	@Then("Enter name on card for Screen19 Scenario1")
	public void enter_name_on_card_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='billingName']")).sendKeys("Rameez");
		Thread.sleep(2000);
	}
	@Then("select region for Screen19 Scenario1")
	public void select_region_for_screen19_scenario1() throws InterruptedException {
		Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='billingCountry']")));
		drpCountry.selectByVisibleText("India");
		Thread.sleep(2000);
	}
	/*@Then("click on save card checkbox for Screen19 Scenario1")
	public void click_on_save_card_checkbox_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
		Thread.sleep(2000);
	}
	@Then("Enter mobile number for Screen19 Scenario1")
	public void enter_mobile_number_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
		Thread.sleep(2000);
	}*/
	@Then("click on pay button for Screen19 Scenario1")
	public void click_on_pay_button_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='SubmitButton-IconContainer']")).click();
		Thread.sleep(4000);
	}
	@Then("click on back to home for Screen19 Scenario1")
	public void click_on_back_to_home_for_screen19_scenario1() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Zurück zur Startseite']")).click();
		Thread.sleep(2000);
		driver.close();
	}
	






	
////////////////////////////////////////////////////////////////////////////////////
	
	//@Register2 @all @sc2
	
	@SuppressWarnings("deprecation")
	@Given("Open the browser")
	public void open_the_browser() {
		System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		
		//options.addArguments("headless");
		//options.addArguments("disable-gpu");
		
		driver = new ChromeDriver(options);
		driver.get("https://devpro2-alpinistcrm.cs173.force.com/#/register");
		
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
	}
	@Then("Click Enter Your Swiss Pass ID button for Register Scenario2")
	public void click_enter_your_swiss_pass_id_button_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='swiss-button']")).click();
		//Thread.sleep(2000);
	}
	@Then("Click input swiss pass for Register Scenario2")
	public void click_input_swiss_pass_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='swiss']")).click();
		//Thread.sleep(2000);
	}
	@Then("Enter Swiss Pass Number for Register Scenario2")
	public void enter_swiss_pass_number_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='swiss']")).sendKeys("S29526357245");
		//Thread.sleep(2000);
	}
	@Then("Click zip code for Register Scenario2")
	public void click_zip_code_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='codenumber']")).click();
		//Thread.sleep(2000);
	}
	@Then("Enter ZIP Code of User for Register Scenario2")
	public void enter_zip_code_of_user_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='codenumber']")).sendKeys("3000");
		//Thread.sleep(2000);
	}
	@Then("Click  Add Swiss Pass ID button for Register Scenario2")
	public void click_add_swiss_pass_id_button_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='next']")).click();
		Thread.sleep(2000);
	}
	/*@Then("Click Allow button for Register Scenario2")
	public void click_allow_button_for_register_scenario2() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Allow']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//Thread.sleep(2000);
		}
	
	@Then("Enter the firstname for Register Scenario2")
	public void enter_the_firstname_for_register_scenario2() {
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rameez");
	}
	@Then("Enter the lastname for Register Scenario2")
	public void enter_the_lastname_for_register_scenario2() {
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Hameed");
	}*/
	@Then("click email field for Register Scenario2")
	public void click_email_field_for_register_scenario2() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Email']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}
	@Then("Enter the email field + symbol for Register Scenario2")
	public void enter_the_email_field_symbol_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("arynla36@gmail.com");
		Thread.sleep(2000);
	}
	@Then("Choose the DOB for Register Scenario2")
	public void choose_the_dob_for_register_scenario2() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@height='16']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}
	@Then("Choose the DOBYear for Register Scenario2")
	public void choose_the_dob_year_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
		Thread.sleep(2000);
	}
	@Then("Choose the DOBMonth for Register Scenario2")
	public void choose_the_dob_month_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("Choose the DOBDate for Register Scenario2")
	public void choose_the_dob_date_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
		Thread.sleep(2000);	
	}
	@Then("click the Confirm Date button for Register Scenario2")
	public void click_the_confirm_date_button_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//span[contains(text(),'Confirm Date')]")).click();
		Thread.sleep(2000);
	}
	@Then("Enter and enter the password for Register Scenario2")
	public void enter_and_enter_the_password_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
		Thread.sleep(2000);
	}
	@Then("Enter and enter the confirm password for Register Scenario2")
	public void enter_and_enter_the_confirm_password_for_register_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
		Thread.sleep(2000);
	}
	@Then("click the Continue button for Register Scenario2")
	public void click_the_continue_button_for_register_scenario2() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Continue')]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}
	@Then("click the Next button for Register Screen2 Scenario2")
	public void click_the_next_button_for_register_screen2_scenario2() {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
	}
	@Then("click second location for Register Screen3 Scenario2")
	public void click_second_location_for_register_screen3_scenario2() {
		driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[2]")).click();
	}
	@Then("click the Next button for Register Screen3 Scenario2")
	public void click_the_next_button_for_register_screen3_scenario2() {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
	}
	@Then("click the gender for Register Screen4 Scenario2")
	public void click_the_gender_for_register_screen4_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("Slide the Height for Register Screen4 Scenario2")
	public void slide_the_height_for_register_screen4_scenario2() {
		WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i <= 133 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }	}
	@Then("Slide the Weight for Register Screen4 Scenario2")
	public void slide_the_weight_for_register_screen4_scenario2() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i < 65 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	@Then("Slide the Shoe Size for Register Screen4 Scenario2")
	public void slide_the_shoe_size_for_register_screen4_scenario2() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 15; i < 26 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	/*@Then("Enter the Ski boots size fo without decimal value for Register Screen4 Scenario2")
	public void enter_the_ski_boots_size_fo_without_decimal_value_for_register_screen4_scenario2() {
		driver.findElement(By.xpath("//input[@type='number']")).sendKeys("7.5");
	}*/
	@Then("click the Continue button for Register Screen4 Scenario2")
	public void click_the_continue_button_for_register_screen4_scenario2() {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
	}

	@Then("click the Skill type for Register Screen5 Scenario2")
	public void click_the_skill_type_for_register_screen5_scenario2() {
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[2]")).click();
	}
	@Then("Slide the Select Skill for Register Screen5 Scenario2")
	public void slide_the_select_skill_for_register_screen5_scenario2() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
		
		for (int i = 0; i < 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
            }
	}
	@Then("Slide the Select Gear grade for Register Screen5 Scenario2")
	public void slide_the_select_gear_grade_for_register_screen5_scenario2() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 0; i < 1 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	@Then("click the Continue button for Register Screen5 Scenario2")
	public void click_the_continue_button_for_register_screen5_scenario2() {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
	}

	@Then("click the Transport typeCfor Register Screen6 Scenario2")
	public void click_the_transport_type_cfor_register_screen6_scenario2() {
		driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[2]")).click();
	}
	/*@Then("Enter the number plate value for Register Screen6 Scenario2")
	public void enter_the_number_plate_value_for_register_screen6_scenario2() {
		driver.findElement(By.xpath("//input[@name='license']")).sendKeys("TN38AA");
	}*/
	@Then("click the Next button for Register Screen6 Scenario2")
	public void click_the_next_button_for_register_screen6_scenario2() {
		driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	}

	@Then("Slide the Accomodation for Register Screen7 Scenario2")
	public void slide_the_accomodation_for_register_screen7_scenario2() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 1; i < 1 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	@Then("click the Next button for Register Screen7 Scenario2")
	public void click_the_next_button_for_register_screen7_scenario2() {
		driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	}

	@Then("Enter the Street for Register Screen8 Scenario2")
	public void enter_the_street_for_register_screen8_scenario2() {
		driver.findElement(By.xpath("//input[@id='street']")).sendKeys("Pammal");
	}
	@Then("Enter the Number for Register Screen8 Scenario2")
	public void enter_the_number_for_register_screen8_scenario2() {
		driver.findElement(By.xpath("//input[@id='number']")).sendKeys("135");
	}
	@Then("Enter the Zip code for Register Screen8 Scenario2")
	public void enter_the_zip_code_for_register_screen8_scenario2() {
		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("55221");
	}
	@Then("Enter the City for Register Screen8 Scenario2")
	public void enter_the_city_for_register_screen8_scenario2() {
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Chennai");
	}
	@Then("Choose the Country for Register Screen8 Scenario2")
	public void choose_the_country_for_register_screen8_scenario2() {
		Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='countrySelect']")));
		drpCountry.selectByVisibleText("🇮🇳 India");
	}
	@Then("click the Continue button for Register Screen8 Scenario2")
	public void click_the_continue_button_for_register_screen8_scenario2() {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
	}

	@Then("Click the next button for Register Screen9 Scenario2")
	public void click_the_next_button_for_register_screen9_scenario2() {
		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
	}

	@Then("Click the first checkboxes for Register Screen10 Scenario2")
	public void click_the_first_checkboxes_for_register_screen10_scenario2() {
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
	}
	@Then("Click the second checkboxes for Register Screen10 Scenario2")
	public void click_the_second_checkboxes_for_register_screen10_scenario2() {
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
	}
	@Then("click the Complete Sign Up button for Register Screen10 Scenario2")
	public void click_the_complete_sign_up_button_for_register_screen10_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Complete Sign Up']")).click();
		Thread.sleep(2000);
	}

	/*@Then("Click the promocode checkbox for Register Screen11 Scenario2")
	public void click_the_promocode_checkbox_for_register_screen11_scenario2() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='I have a promo code']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Click promocode for Register Screen11 Scenario2")
	public void click_promocode_for_register_screen11_scenario2() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='promo']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Enter promocode for Register Screen11 Scenario2")
	public void enter_promocode_for_register_screen11_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//input[@name='promo']")).sendKeys("1111122222");
		Thread.sleep(2000);
	}*/
	@Then("click the Continue button for Register Screen11 Scenario2")
	public void click_the_continue_button_for_register_screen11_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
		Thread.sleep(2000);
	}	
	
	@Then("Click Plan a trip button for Screen12 Scenario2")
	public void click_plan_a_trip_button_for_screen12_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
		Thread.sleep(2000);
	}
	
	@Then("Select second location checkbox for Screen13 Scenario2")
	public void select_second_location_checkbox_for_screen13_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Chäserrugg']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen13 Scenario2")
	public void click_next_button_for_screen13_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		Thread.sleep(2000);
	}

	@Then("Click on start date button for Screen14 Scenario2")
	public void click_on_start_date_button_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Select start date for Screen14 Scenario2")
	public void select_start_date_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='October 25, 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("select End date for Screen14 Scenario2")
	public void select_end_date_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='October 28, 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("Click start time dropdown for Screen14 Scenario2")
	public void click_start_time_dropdown_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
		Thread.sleep(2000);
		
	}
	@Then("select start time for Screen14 Scenario2")
	public void select_start_time_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='11:00'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Click End time dropdown for Screen14 Scenario2")
	public void click_end_time_dropdown_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("select end timefor Screen14 Scenario2")
	public void select_end_timefor_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='18:00'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("Click confirm button for Screen14 Scenario2")
	public void click_confirm_button_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen14 Scenario2")
	public void click_next_button_for_screen14_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		Thread.sleep(2000);
	}

	@Then("Select second equipment for Screen15 Scenario2")
	public void select_second_equipment_for_screen15_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
		Thread.sleep(2000);
	}
	/*@Then("Select second equipment for Screen15 Scenario1")
	public void select_second_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
	}
	@Then("Select third equipment for Screen15 Scenario1")
	public void select_third_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
	}
	@Then("Select fourth equipment for Screen15 Scenario1")
	public void select_fourth_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
	}*/
	@Then("Click Yes button for Screen15 Scenario2")
	public void click_yes_button_for_screen15_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='next button-style']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen16 Scenario2")
	public void click_next_button_for_screen16_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Next Step']")).click();
		Thread.sleep(2000);
	}
	
	@Then("Click checkbox for Screen17 Scenario2")
	public void click_checkbox_for_screen17_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Send me a link to the webshop after I completed my Alpinist booking ']")).click();
		Thread.sleep(2000);
	}

	@Then("Click yes button for Screen17 Scenario2")
	public void click_yes_button_for_screen17_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='next']")).click();
		Thread.sleep(2000);
	}

	@Then("Click Payment button for Screen18 Scenario2")
	public void click_payment_button_for_screen18_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
		Thread.sleep(5000);
		driver.close();
	}

	
	
///////////////////////////////////////////////////////////////////////////////////
	
	//@Register3 @all @sc3
	
	@SuppressWarnings("deprecation")
	@Given("Browser to be open")
	public void browser_to_be_open() {
		System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		
		//options.addArguments("headless");
		//options.addArguments("disable-gpu");
		
		driver = new ChromeDriver(options);
		driver.get("https://devpro2-alpinistcrm.cs173.force.com/#/register");
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
	}
	@Then("Click Enter Your Swiss Pass ID button for Register Scenario3")
	public void click_enter_your_swiss_pass_id_button_for_register_scenario3() {
		driver.findElement(By.xpath("//div[@class='swiss-button']")).click();
	}
	@Then("Click input swiss pass for Register Scenario3")
	public void click_input_swiss_pass_for_register_scenario3() {
		driver.findElement(By.xpath("//input[@id='swiss']")).click();
	}
	@Then("Enter Swiss Pass Number for Register Scenario3")
	public void enter_swiss_pass_number_for_register_scenario3() {
		driver.findElement(By.xpath("//input[@id='swiss']")).sendKeys("S29526357245");
	}
	@Then("Click zip code for Register Scenario3")
	public void click_zip_code_for_register_scenario3() {
		driver.findElement(By.xpath("//input[@id='codenumber']")).click();
	}
	@Then("Enter ZIP Code of User for Register Scenario3")
	public void enter_zip_code_of_user_for_register_scenario3() {
		driver.findElement(By.xpath("//input[@id='codenumber']")).sendKeys("3000");
	}
	@Then("Click  Add Swiss Pass ID button for Register Scenario3")
	public void click_add_swiss_pass_id_button_for_register_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='next']")).click();
		Thread.sleep(2000);
	}
	@Then("Click Allow button for Register Scenario3")
	public void click_allow_button_for_register_scenario3() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Allow']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}
	@Then("Enter the email field for Register Scenario13")
	public void enter_the_email_field_for_register_scenario3() {
		driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("t_124@yahoo.com");
	}
	@Then("Choose the DOB for Register Scenario3")
	public void choose_the_dob_for_register_scenario3() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@height='16']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}
	@Then("Choose the DOBYear for Register Scenario3")
	public void choose_the_dob_year_for_register_scenario3() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
	}
	@Then("Choose the DOBMonth for Register Scenario3")
	public void choose_the_dob_month_for_register_scenario3() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
	}
	@Then("Choose the DOBDate for Register Scenario3")
	public void choose_the_dob_date_for_register_scenario3() {
		driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
	}
	@Then("click the Confirm Date button for Register Scenario3")
	public void click_the_confirm_date_button_for_register_scenario3() {
		driver.findElement(By.xpath("//span[contains(text(),'Confirm Date')]")).click();
	}
	@Then("Enter and enter the password for Register Scenario3")
	public void enter_and_enter_the_password_for_register_scenario3() {
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
	}
	@Then("Enter and enter the confirm password for Register Scenario3")
	public void enter_and_enter_the_confirm_password_for_register_scenario3() {
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
	}
	@Then("click the Continue button for Register Scenario3")
	public void click_the_continue_button_for_register_scenario3() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Continue')]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	}

	@Then("uncheck the first checkbox for Screen2 Scenario3")
	public void uncheck_the_first_checkbox_for_screen2_scenario3() {
		//System.out.println("Inside step - Click the first checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[1]")).click();
	}

	@Then("uncheck the second checkbox for Screen2 Scenario3")
	public void uncheck_the_second_checkbox_for_screen2_scenario3() {
		//System.out.println("Inside step - Click the Second checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[2]")).click();
	}

	@Then("uncheck the third checkbox for Screen2 Scenario3")
	public void uncheck_the_third_checkbox_for_screen2_scenario3() {
		System.out.println("Inside step - Click the third checkboxes for Register Screen2 Scenario1");
		
		driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[3]")).click();
	}

	@Then("click the Next button for Register Screen2 Scenario3")
	public void click_the_next_button_for_register_screen2_scenario3() {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
	}


	@Then("click third location for Register Screen3 Scenario3")
	public void click_third_location_for_register_screen3_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("click the Next button for Register Screen3 Scenario3")
	public void click_the_next_button_for_register_screen3_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		Thread.sleep(2000);
	}
	
	/*@Then("click the gender for Register Screen4 Scenario3")
	public void click_the_gender_for_register_screen4_scenario3() {
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
	}
	@Then("Slide the Height for Register Screen4 Scenario3")
	public void slide_the_height_for_register_screen4_scenario3() {
		WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i <= 130 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	@Then("Slide the Weight for Register Screen4 Scenario3")
	public void slide_the_weight_for_register_screen4_scenario3() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i <= 50 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
	}
	@Then("Slide the Shoe Size for Register Screen4 Scenario3")
	public void slide_the_shoe_size_for_register_screen4_scenario3() throws InterruptedException {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 15; i <= 30 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
	}
	@Then("Enter the Ski boots size fo with decimal value for Register Screen4 Scenario3")
	public void enter_the_ski_boots_size_fo_with_decimal_value_for_register_screen4_scenario3() {
		driver.findElement(By.xpath("//input[@type='number']")).sendKeys("7");
	}
	@Then("click the Continue button for Register Screen4 Scenario3")
	public void click_the_continue_button_for_register_screen4_scenario3() {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
	}


	@Then("click the Skill type for Register Screen5 Scenario3")
	public void click_the_skill_type_for_register_screen5_scenario3() {
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[2]")).click();
	}
	@Then("Slide the Select Skill for Register Screen5 Scenario3")
	public void slide_the_select_skill_for_register_screen5_scenario3() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
		
		for (int i = 0; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
            }
	}
	@Then("Slide the Select Gear grade for Register Screen5 Scenario3")
	public void slide_the_select_gear_grade_for_register_screen5_scenario3() {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 0; i <= 1 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
		}
	}
	@Then("click the Continue button for Register Screen5 Scenario3")
	public void click_the_continue_button_for_register_screen5_scenario3() {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
	}*/

	@Then("click the car transport for Screen6 Scenario3")
	public void click_the_car_transport_for_Screen6_Scenario3() {
		driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[1]")).click();
	}
	@Then("click the train transport for Screen6 Scenario3")
	public void click_the_train_transport_for_Screen6_Scenario3() {
		driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[2]")).click();
	}
	@Then("Enter the number plate value for Register Screen6 Scenario3")
	public void enter_the_number_plate_value_for_register_screen6_scenario3() {
		driver.findElement(By.xpath("//input[@name='license']")).sendKeys("TN38AA");
	}
	@Then("click the Next button for Register Screen6 Scenario3")
	public void click_the_next_button_for_register_screen6_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()=' Next ']")).click();
		Thread.sleep(2000);
	}

	/*@Then("Slide the Accomodation for Register Screen7 Scenario3")
	public void slide_the_accomodation_for_register_screen7_scenario3() throws InterruptedException {
		WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 0; i <= 1 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
            
        }
		Thread.sleep(2000);
	}
	@Then("click the Next button for Register Screen7 Scenario3")
	public void click_the_next_button_for_register_screen7_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()=' Next ']")).click();
		Thread.sleep(2000);
	}*/

	@Then("Enter the Street for Register Screen8 Scenario3")
	public void enter_the_street_for_register_screen8_scenario3() {
		driver.findElement(By.xpath("//input[@id='street']")).sendKeys("Pammal");	
	}
	@Then("Enter the Number for Register Screen8 Scenario3")
	public void enter_the_number_for_register_screen8_scenario3() {
		driver.findElement(By.xpath("//input[@id='number']")).sendKeys("135");
	}
	/*@Then("Enter the Zip code for Register Screen8 Scenario3")
	public void enter_the_zip_code_for_register_screen8_scenario3() {
		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("55221");
	}
	@Then("Enter the City for Register Screen8 Scenario3")
	public void enter_the_city_for_register_screen8_scenario3() {
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Chennai");
	}
	@Then("Choose the Country for Register Screen8 Scenario3")
	public void choose_the_country_for_register_screen8_scenario3() {
		Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='countrySelect']")));
		drpCountry.selectByVisibleText("🇮🇳 India");
	}*/
	@Then("click the Continue button for Register Screen8 Scenario3")
	public void click_the_continue_button_for_register_screen8_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
		Thread.sleep(2000);
	}
	@Then("uncheck the first box for Screen9 Scenario3")
	public void uncheck_the_first_box_for_Screen9_Scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='checkbox-text'])[1]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("uncheck the second box for Screen9 Scenario3")
	public void uncheck_the_second_box_for_Screen9_Scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='checkbox-text'])[2]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("uncheck the third box for Screen9 Scenario3")
	public void uncheck_the_third_box_for_Screen9_Scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='checkbox-text'])[3]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("uncheck the fourth box for Screen9 Scenario3")
	public void uncheck_the_fourth_box_for_Screen9_Scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='checkbox-text'])[4]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("uncheck the fifth box for Screen9 Scenario3")
	public void uncheck_the_fifth_box_for_Screen9_Scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='checkbox-text'])[5]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}

	@Then("Click the next button for Register Screen9 Scenario3")
	public void click_the_next_button_for_register_screen9_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
		Thread.sleep(2000);
	}

	@Then("Click the first checkboxes for Register Screen10 Scenario3")
	public void click_the_first_checkboxes_for_register_screen10_scenario3() {
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
	}
	@Then("Click the second checkboxes for Register Screen10 Scenario3")
	public void click_the_second_checkboxes_for_register_screen10_scenario3() {
		driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
	}
	@Then("click the Complete Sign Up button for Register Screen10 Scenario3")
	public void click_the_complete_sign_up_button_for_register_screen10_scenario3() {
		driver.findElement(By.xpath("//button[text()='Complete Sign Up']")).click();
	}
	/*@Then("Click the promocode checkbox for Register Screen11 Scenario3")
	public void click_the_promocode_checkbox_for_register_screen11_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='I have a promo code']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Click promocode for Register Screen11 Scenario3")
	public void click_promocode_for_register_screen11_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='promo']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Enter promocode for Register Screen11 Scenario3")
	public void enter_promocode_for_register_screen11_scenario3() {
		driver.findElement(By.xpath("//input[@name='promo']")).sendKeys("1111122222");
	}*/
	@Then("click the Continue button for Register Screen11 Scenario3")
	public void click_the_continue_button_for_register_screen11_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
		Thread.sleep(2000);
		
	}
	@Then("click the Continue button2 for Register Screen11 Scenario3")
	public void click_the_continue_button2_for_register_screen11_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue']")).click();
		Thread.sleep(2000);
		
	}
	@Then("Click Plan a trip button for Screen12 Scenario3")
	public void click_plan_a_trip_button_for_screen12_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Plan a Trip Now ']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	
	@Then("Select third location checkbox for Screen13 Scenario3")
	public void select_third_location_checkbox_for_screen13_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Pizol']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen13 Scenario3")
	public void click_next_button_for_screen13_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		Thread.sleep(2000);
	}

	@Then("Click on start date button for Screen14 Scenario3")
	public void click_on_start_date_button_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Select start date for Screen14 Scenario3")
	public void select_start_date_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='October 25, 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("select End date for Screen14 Scenario3")
	public void select_end_date_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//td[@aria-label='October 28, 2021']")).click();
		Thread.sleep(2000);
	}
	@Then("Click start time dropdown for Screen14 Scenario3")
	public void click_start_time_dropdown_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
		Thread.sleep(2000);
		
	}
	@Then("select start time for Screen14 Scenario3")
	public void select_start_time_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='11:00'])[1]")).click();
		Thread.sleep(2000);
	}
	@Then("Click End time dropdown for Screen14 Scenario3")
	public void click_end_time_dropdown_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("select end timefor Screen14 Scenario3")
	public void select_end_timefor_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("(//option[@value='18:00'])[2]")).click();
		Thread.sleep(2000);
	}
	@Then("Click confirm button for Screen14 Scenario3")
	public void click_confirm_button_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen14 Scenario3")
	public void click_next_button_for_screen14_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		Thread.sleep(2000);
	}

	@Then("Select first equipment for Screen15 Scenario3")
	public void select_first_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select second equipment for Screen15 Scenario3")
	public void select_second_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Boots']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select third equipment for Screen15 Scenario3")
	public void select_third_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Jacket']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select fourth equipment for Screen15 Scenario3")
	public void select_fourth_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Pant']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select fifth equipment for Screen15 Scenario3")
	public void select_fifth_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Poles']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select sixth equipment for Screen15 Scenario3")
	public void select_sixth_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Gloves']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select seventh equipment for Screen15 Scenario3")
	public void select_seventh_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Helmet']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select eighth equipment for Screen15 Scenario3")
	public void select_eighth_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Goggle']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	@Then("Select nineth equipment for Screen15 Scenario3")
	public void select_nineth_equipment_for_screen15_scenario3() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
	}
	/*@Then("Select second equipment for Screen15 Scenario1")
	public void select_second_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
	}
	@Then("Select third equipment for Screen15 Scenario1")
	public void select_third_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
	}
	@Then("Select fourth equipment for Screen15 Scenario1")
	public void select_fourth_equipment_for_screen15_scenario1() {
		driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
	}*/
	@Then("Click Yes button for Screen15 Scenario3")
	public void click_yes_button_for_screen15_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='next button-style']")).click();
		Thread.sleep(2000);
	}
	@Then("Click next button for Screen16 Scenario3")
	public void click_next_button_for_screen16_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()='Next Step']")).click();
		Thread.sleep(2000);
	}
	
	/*@Then("Click checkbox for Screen17 Scenario2")
	public void click_checkbox_for_screen17_scenario2() throws InterruptedException {
		driver.findElement(By.xpath("//p[text()='Send me a link to the webshop after I completed my Alpinist booking ']")).click();
		Thread.sleep(2000);
	}*/

	@Then("Click yes button for Screen17 Scenario3")
	public void click_yes_button_for_screen17_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[@class='next']")).click();
		Thread.sleep(2000);
	}

	@Then("Click Payment button for Screen18 Scenario3")
	public void click_payment_button_for_screen18_scenario3() throws InterruptedException {
		driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
		Thread.sleep(5000);
		driver.close();
	}
		

////////////////////////////////////////////////////////////////////////////////////
	
	//@Register4 @all @sc4
	
@Given("open browser")
public void open_browser() {
	
	
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://partial1-alpinistcrmuat.cs101.force.com/#/register");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	
	//WebDriverWait wait = new WebDriverWait(driver, 20);
	
	//driver.manage().window().maximize();
}
/*@Given("Navigate to Alpinist URL")
public void navigate_to_alpinist_url() throws InterruptedException {
	driver.navigate().to("https://devpro1-alpinistcrm.cs173.force.com/#/register");
	//Thread.sleep(2000);
}*/
@Then("Enter the firstname for Register Scenario4")
public void enter_the_firstname_for_register_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Enter the firstname for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rameez");
	//Thread.sleep(2000);
}
@Then("Enter the lastname for Register Scenario4")
public void enter_the_lastname_for_register_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Enter the lastname for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Hameed");	
	//Thread.sleep(2000);
}
@Then("Enter the email field for Register Scenario4")
public void enter_the_email_field_for_register_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Enter the email field for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("t421@yahoo.com");
	//Thread.sleep(2000);
}
@Then("Choose the DOB for Register Scenario4")
public void choose_the_dob_for_register_scenario4() {
	driver.findElement(By.xpath("//div[@class='single-date-picker flex align-center']")).click();
}
@Then("Choose the DOBYear for Register Scenario4")
public void choose_the_dob_year_for_register_scenario4() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
}
@Then("Choose the DOBMonth for Register Scenario4")
public void choose_the_dob_month_for_register_scenario4() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
}
@Then("Choose the DOBDate for Register Scenario4")
public void choose_the_dob_date_for_register_scenario4() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
}
@Then("click the Confirm Date button for Register Scenario4")
public void click_the_confirm_date_button_for_register_scenario4() {
	driver.findElement(By.xpath("//span[contains(text(),'Confirm Date')]")).click();
}
@Then("Enter and enter the password for Register Scenario4")
public void enter_and_enter_the_password_for_register_scenario4() {
	driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
}
@Then("Enter and enter the confirm password for Register Scenario4")
public void enter_and_enter_the_confirm_password_for_register_scenario4() {
	driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
}
@Then("click the Continue button for Register Scenario4")
public void click_the_continue_button_for_register_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[contains(text(),'Continue')]")).click();
	Thread.sleep(2000);
}




/*@Then("Click the first checkboxes for Register Screen2 Scenario1")
public void click_the_first_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the first checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[1]")).click();
}

@Then("Click the Second checkboxes for Register Screen2 Scenario1")
public void click_the_second_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the Second checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[2]")).click();
}

@Then("Click the third checkboxes for Register Screen2 Scenario1")
public void click_the_third_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the third checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[3]")).click();
}*/

@Then("click the Next button for Register Screen2 Scenario4")
public void click_the_next_button_for_register_screen2_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("click first location for Register Screen3 Scenario4")
public void click_first_location_for_register_screen3_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click any location for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[1]")).click();
	Thread.sleep(2000);
}
@Then("click second location for Register Screen3 Scenario4")
public void click_second_location_for_register_screen3_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click any location for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[2]")).click();
	Thread.sleep(2000);
}
@Then("click third location for Register Screen3 Scenario4")
public void click_third_location_for_register_screen3_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click any location for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[3]")).click();
	Thread.sleep(2000);
}

@Then("click the Next button for Register Screen3 Scenario4")
public void click_the_next_button_for_register_screen3_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("click the gender for Register Screen4 Scenario4")
public void click_the_gender_for_register_screen4_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the gender for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[3]")).click();
	Thread.sleep(2000);
}


@Then("Slide the Height for Register Screen4 Scenario4")
public void slide_the_height_for_register_screen4_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Height for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
	
	for (int i = 0; i <= 85 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("Slide the Weight for Register Screen4 Scenario4")
public void slide_the_weight_for_register_screen4_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Weight for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
	
	for (int i = 0; i <= 60 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("Slide the Shoe Size for Register Screen4 Scenario4")
public void slide_the_shoe_size_for_register_screen4_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Shoe Size for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
	
	for (int i = 24; i <= 42 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

/*@Then("Enter the Ski boots size for Register Screen4 Scenario1")
public void Enter_the_Ski_boots_size_for_Register_Screen4_Scenario1() throws InterruptedException {
	//System.out.println("Inside step - Enter the Ski boots size for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("//input[@type='number']")).sendKeys("");
	
	Thread.sleep(2000);
}*/

@Then("click the Continue button for Register Screen4 Scenario4")
public void click_the_continue_button_for_register_screen4_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	
	Thread.sleep(2000);
}

@Then("click the Skill type for Register Screen5 Scenario4")
public void click_the_skill_type_for_register_screen5_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Skill type for Register Screen5 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
	Thread.sleep(2000);
	}

@Then("Slide the Select Skill for Register Screen5 Scenario4")
public void slide_the_select_skill_for_register_screen5_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Select Skill for Register Screen5 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
	
	for (int i = 1; i < 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

@Then("Slide the Select Gear grade for Register Screen5 Scenario4")
public void slide_the_select_gear_grade_for_register_screen5_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Select Gear grade for Register Screen5 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
	
	for (int i = 1; i < 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

@Then("click the Continue button for Register Screen5 Scenario4")
public void click_the_continue_button_for_register_screen5_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen5 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(2000);
	}

@Then("click the Transport type for Register Screen6 Scenario4")
public void click_the_transport_type_for_register_screen6_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Transport type for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[2]")).click();
	Thread.sleep(2000);
	}

@Then("click the Next button for Register Screen6 Scenario4")
public void click_the_next_button_for_register_screen6_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	Thread.sleep(2000);
}

@Then("Slide the Accomodation for Register Screen7 Scenario4")
public void slide_the_accomodation_for_register_screen7_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Slide the Accomodation for Register Screen7 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
	
	for (int i = 1; i < 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("click the Next button for Register Screen7 Scenario4")
public void click_the_next_button_for_register_screen7_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen7 Scenario1");
	
	driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	Thread.sleep(2000);
}

@Then("Enter the Zip code for Register Screen8 Scenario4")
public void enter_the_zip_code_for_register_screen8_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Enter the Zip code for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("640007");
	Thread.sleep(2000);
}

@Then("click the Continue button for Register Screen8 Scenario4")
public void click_the_continue_button_for_register_screen8_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(2000);
}


@Then("Click the next button for Register Screen9 Scenario4")
public void Click_the_next_button_for_Register_Screen9_Scenario4() throws InterruptedException {
	//System.out.println("Inside step - Click the next button for Register Screen9 Scenario1");
	
	driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
	Thread.sleep(2000);
}

@Then("Click the first checkboxes for Register Screen10 Scenario4")
public void click_the_first_checkboxes_for_register_screen10_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Click the first checkboxes for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
	Thread.sleep(2000);
}

@Then("Click the second checkboxes for Register Screen10 Scenario4")
public void click_the_second_checkboxes_for_register_screen10_scenario4() throws InterruptedException {
	//System.out.println("Inside step - Click the second checkboxes for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
	Thread.sleep(2000);
}

@Then("click the Complete Sign Up button for Register Screen10 Scenario4")
public void click_the_complete_sign_up_button_for_register_screen10_scenario4() throws InterruptedException {
    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Complete Sign Up']")).click();
	Thread.sleep(2000);

	
}

@Then("click the Continue button for Register Screen11 Scenario4")
public void click_the_continue_button_for_register_screen11_scenario4() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen11 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(3000);
}

@Then("Click Plan a trip button for Screen12 Scenario4")
public void click_plan_a_trip_button_for_screen12_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}

@Then("Select first location checkbox for Screen13 Scenario4")
public void select_first_location_checkbox_for_screen13_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Braunwald']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen13 Scenario4")
public void click_next_button_for_screen13_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("Click on start date button for Screen14 Scenario4")
public void click_on_start_date_button_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Select start date for Screen14 Scenario4")
public void select_start_date_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 20, 2021']")).click();
	Thread.sleep(2000);
}
@Then("select End date for Screen14 Scenario4")
public void select_end_date_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 22, 2021']")).click();
	Thread.sleep(2000);
}
@Then("Click start time dropdown for Screen14 Scenario4")
public void click_start_time_dropdown_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
	Thread.sleep(2000);
	
}
@Then("select start time for Screen14 Scenario4")
public void select_start_time_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Click End time dropdown for Screen14 Scenario4")
public void click_end_time_dropdown_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
	Thread.sleep(2000);
}
@Then("select end timefor Screen14 Scenario4")
public void select_end_timefor_screen14_scenari41() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
	Thread.sleep(2000);
}
@Then("Click confirm button for Screen14 Scenario4")
public void click_confirm_button_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen14 Scenario4")
public void click_next_button_for_screen14_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("Select first equipment for Screen15 Scenario4")
public void select_first_equipment_for_screen15_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski']")).click();
	Thread.sleep(2000);
}
@Then("Select second equipment for Screen15 Scenario4")
public void select_second_equipment_for_screen15_scenario4() {
	driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
}
/*@Then("Select third equipment for Screen15 Scenario1")
public void select_third_equipment_for_screen15_scenario1() {
	driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
}
@Then("Select fourth equipment for Screen15 Scenario1")
public void select_fourth_equipment_for_screen15_scenario1() {
	driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
}*/
@Then("click on add message for Screen15 Scenario4")
public void click_on_add_message_for_screen15_scenario4() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Add Message to Store']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("click message box for Screen15 Scenario4")
public void click_message_box_for_screen15_scenario4() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea[@id='message']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("enter message for Screen15 Scenario4")
public void enter_message_for_screen15_scenario4() {
	driver.findElement(By.xpath("//textarea[@name='message']")).sendKeys("No need any other help");
}
@Then("Click Yes button for Screen15 Scenario4")
public void click_yes_button_for_screen15_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='next button-style']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen16 Scenario4")
public void click_next_button_for_screen16_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Next Step']")).click();
	Thread.sleep(2000);
}

/*@Then("Click checkbox for Screen17 Scenario4")
public void click_checkbox_for_screen17_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//p[contains(text(),' Send me a link')]")).click();
	Thread.sleep(2000);
}*/

@Then("Click yes button for Screen17 Scenario4")
public void click_yes_button_for_screen17_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()=' Yes, Add Ski Pass ']")).click();
	Thread.sleep(2000);
}

@Then("Click Payment button for Screen18 Scenario4")
public void click_payment_button_for_screen18_scenario4() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
	Thread.sleep(5000);
	driver.close();
}


/*@Then("Select payment option for Screen19 Scenario1")
public void select_payment_option_for_screen19_scenario1() {
	driver.findElement(By.xpath("//button[text()=' Choose Payment Method ']")).click();
}
@Then("Click back to home for Screen19 Scenario1")
public void click_back_to_home_for_screen19_scenario1() {
	driver.findElement(By.xpath("//button[text()='Back to Home']")).click();
	
	//driver.close();
}*/

///////////////////////////////////////////////////////////////////////////////

	//@all @sc5

@SuppressWarnings("deprecation")
@Given("browser open")
public void browser_open() {
	
	
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://partial1-alpinistcrmuat.cs101.force.com/#/register");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	
	//WebDriverWait wait = new WebDriverWait(driver, 20);
	
	//driver.manage().window().maximize();
}
/*@Given("Navigate to Alpinist URL")
public void navigate_to_alpinist_url() throws InterruptedException {
	driver.navigate().to("https://devpro1-alpinistcrm.cs173.force.com/#/register");
	//Thread.sleep(2000);
}*/
@Then("Enter the firstname for Register Scenario5")
public void enter_the_firstname_for_register_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the firstname for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rameez");
	//Thread.sleep(2000);
}
@Then("Enter the lastname for Register Scenario5")
public void enter_the_lastname_for_register_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the lastname for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Hameed");	
	//Thread.sleep(2000);
}
@Then("Enter the email field for Register Scenario5")
public void enter_the_email_field_for_register_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the email field for Register Scenario1");
	
	driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("t266@yahoo.com");
	//Thread.sleep(2000);
}
@Then("Choose the DOB for Register Scenario5")
public void choose_the_dob_for_register_scenario5() {
	driver.findElement(By.xpath("//div[@class='single-date-picker flex align-center']")).click();
}
@Then("Choose the DOBYear for Register Scenario5")
public void choose_the_dob_year_for_register_scenario5() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
}
@Then("Choose the DOBMonth for Register Scenario5")
public void choose_the_dob_month_for_register_scenario5() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
}
@Then("Choose the DOBDate for Register Scenario5")
public void choose_the_dob_date_for_register_scenario5() {
	driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
}
@Then("click the Confirm Date button for Register Scenario5")
public void click_the_confirm_date_button_for_register_scenario5() {
	driver.findElement(By.xpath("//span[contains(text(),'Confirm Date')]")).click();
}
@Then("Enter and enter the password for Register Scenario5")
public void enter_and_enter_the_password_for_register_scenario5() {
	driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
}
@Then("Enter and enter the confirm password for Register Scenario5")
public void enter_and_enter_the_confirm_password_for_register_scenario5() {
	driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
}
@Then("click the Continue button for Register Scenario5")
public void click_the_continue_button_for_register_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[contains(text(),'Continue')]")).click();
	Thread.sleep(2000);
}




/*@Then("Click the first checkboxes for Register Screen2 Scenario1")
public void click_the_first_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the first checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[1]")).click();
}

@Then("Click the Second checkboxes for Register Screen2 Scenario1")
public void click_the_second_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the Second checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[2]")).click();
}

@Then("Click the third checkboxes for Register Screen2 Scenario1")
public void click_the_third_checkboxes_for_register_screen2_scenario1() {
	System.out.println("Inside step - Click the third checkboxes for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-wrap'])[3]")).click();
}*/

@Then("click the Next button for Register Screen2 Scenario5")
public void click_the_next_button_for_register_screen2_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen2 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("click first location for Register Screen3 Scenario5")
public void click_first_location_for_register_screen3_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click any location for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[1]")).click();
	Thread.sleep(2000);
}

@Then("click the Next button for Register Screen3 Scenario5")
public void click_the_next_button_for_register_screen3_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen3 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("click the gender for Register Screen4 Scenario5")
public void click_the_gender_for_register_screen4_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the gender for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
	Thread.sleep(2000);
}


@Then("Slide the Height for Register Screen4 Scenario5")
public void slide_the_height_for_register_screen4_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Height for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
	
	for (int i = 0; i <= 130 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("Slide the Weight for Register Screen4 Scenario5")
public void slide_the_weight_for_register_screen4_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Weight for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
	
	for (int i = 0; i <= 50 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("Slide the Shoe Size for Register Screen4 Scenario5")
public void slide_the_shoe_size_for_register_screen4_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Shoe Size for Register Screen4 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
	
	for (int i = 24; i <= 30 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

/*@Then("Enter the Ski boots size for Register Screen4 Scenario1")
public void Enter_the_Ski_boots_size_for_Register_Screen4_Scenario1() throws InterruptedException {
	//System.out.println("Inside step - Enter the Ski boots size for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("//input[@type='number']")).sendKeys("");
	
	Thread.sleep(2000);
}*/

@Then("click the Continue button for Register Screen4 Scenario5")
public void click_the_continue_button_for_register_screen4_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen4 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	
	Thread.sleep(2000);
}

@Then("click the Skill type for Register Screen5 Scenario5")
public void click_the_skill_type_for_register_screen5_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Skill type for Register Screen5 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
	Thread.sleep(2000);
	}

@Then("Slide the Select Skill for Register Screen5 Scenario5")
public void slide_the_select_skill_for_register_screen5_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Select Skill for Register Screen5 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
	
	for (int i = 1; i <= 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

@Then("Slide the Select Gear grade for Register Screen5 Scenario5")
public void slide_the_select_gear_grade_for_register_screen5_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Select Gear grade for Register Screen5 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
	
	for (int i = 1; i <= 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	
	Thread.sleep(2000);
}

@Then("click the Continue button for Register Screen5 Scenario5")
public void click_the_continue_button_for_register_screen5_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen5 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(2000);
	}

@Then("click the Transport type for Register Screen6 Scenario5")
public void click_the_transport_type_for_register_screen6_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Transport type for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[1]")).click();
	Thread.sleep(2000);
	}

@Then("Enter the number plate value for Register Screen6 Scenario5")
public void Enter_the_number_plate_value_for_Register_Screen6_Scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the number plate value for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("//input[@name='license']")).sendKeys("TN38AA");
	Thread.sleep(2000);
}

/*@Then("Click the first checkboxes for Register Screen6 Scenario1")
public void click_the_first_checkboxes_for_register_screen6_scenario1() {
	System.out.println("Inside step - Click the first checkboxes for Register Screen6 Scenario1");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()=' I have a Half-Fare Ticket ']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
}

@Then("Click the second checkboxes for Register Screen6 Scenario1")
public void click_the_second_checkboxes_for_register_screen6_scenario1() {
	System.out.println("Inside step - Click the first checkboxes for Register Screen6 Scenario1");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()=' I have a GA Travelcard ']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
}

@Then("Enter the Swiss Pass ID for Register Screen6 Scenario1")
public void enter_the_swiss_pass_id_for_register_screen6_scenario1() {
	System.out.println("Inside step - Enter the Swiss Pass ID for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("//input[@name='swissPass']")).sendKeys("123456");
}*/

@Then("click the Next button for Register Screen6 Scenario5")
public void click_the_next_button_for_register_screen6_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen6 Scenario1");
	
	driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	Thread.sleep(2000);
}

@Then("Slide the Accomodation for Register Screen7 Scenario5")
public void slide_the_accomodation_for_register_screen7_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Slide the Accomodation for Register Screen7 Scenario1");
	
	WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
	
	for (int i = 1; i <= 2 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
    }
	Thread.sleep(2000);
}

@Then("click the Next button for Register Screen7 Scenario5")
public void click_the_next_button_for_register_screen7_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Next button for Register Screen7 Scenario1");
	
	driver.findElement(By.xpath("//button[text()=' Next ']")).click();
	Thread.sleep(2000);
}

@Then("Enter the Street for Register Screen8 Scenario5")
public void enter_the_street_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the Street for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//input[@id='street']")).sendKeys("Pammal");
	Thread.sleep(2000);
}

@Then("Enter the Number for Register Screen8 Scenario5")
public void enter_the_number_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the Number for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//input[@id='number']")).sendKeys("135");
	Thread.sleep(2000);
}

@Then("Enter the Zip code for Register Screen8 Scenario5")
public void enter_the_zip_code_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the Zip code for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("640007");
	Thread.sleep(2000);
}

@Then("Enter the City for Register Screen8 Scenario5")
public void enter_the_city_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Enter the City for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Chennai");
	Thread.sleep(2000);
}

@Then("Choose the Country for Register Screen8 Scenario5")
public void choose_the_country_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Choose the Country for Register Screen8 Scenario1");
	
	Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='countrySelect']")));
	drpCountry.selectByVisibleText("🇮🇳 India");
	Thread.sleep(2000);
}

@Then("click the Continue button for Register Screen8 Scenario5")
public void click_the_continue_button_for_register_screen8_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen8 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(2000);
}


@Then("Click the next button for Register Screen9 Scenario5")
public void Click_the_next_button_for_Register_Screen9_Scenario5() throws InterruptedException {
	//System.out.println("Inside step - Click the next button for Register Screen9 Scenario1");
	
	driver.findElement(By.xpath("//button[contains(text(),'Next')]")).click();
	Thread.sleep(2000);
}

@Then("Click the first checkboxes for Register Screen10 Scenario5")
public void click_the_first_checkboxes_for_register_screen10_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Click the first checkboxes for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
	Thread.sleep(2000);
}

@Then("Click the second checkboxes for Register Screen10 Scenario5")
public void click_the_second_checkboxes_for_register_screen10_scenario5() throws InterruptedException {
	//System.out.println("Inside step - Click the second checkboxes for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
	Thread.sleep(2000);
}

@Then("click the Complete Sign Up button for Register Screen10 Scenario5")
public void click_the_complete_sign_up_button_for_register_screen10_scenario5() throws InterruptedException {
    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Complete Sign Up']")).click();
	Thread.sleep(2000);

	
}

/*@Then("Click the checkboxes for Register Screen11 Scenario1")
public void click_the_checkboxes_for_register_screen11_scenario1() {
	System.out.println("Inside step - Click the checkboxes for Register Screen11 Scenario1");
	
	driver.findElement(By.xpath("//div[@class='checkbox-text']")).click();
}*/

@Then("Click the promocode checkbox for Register Screen11 Scenario5")
public void Click_the_promocode_checkbox_for_Register_Screen11_Scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen8 Scenario1");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='I have a promo code']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);

}

@Then("Click promocode for Register Screen11 Scenario5")
public void Click_promocode_for_Register_Screen11_Scenario5() throws InterruptedException {
    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='promo']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}

@Then("Enter promocode for Register Screen11 Scenario5")
public void Enter_promocode_for_Register_Screen11_Scenario5() throws InterruptedException {
    //System.out.println("Inside step - click the Complete Sign Up button for Register Screen10 Scenario1");
	
	driver.findElement(By.xpath("//input[@name='promo']")).sendKeys("1111122222");
	Thread.sleep(2000);
}

@Then("click the Continue button for Register Screen11 Scenario5")
public void click_the_continue_button_for_register_screen11_scenario5() throws InterruptedException {
	//System.out.println("Inside step - click the Continue button for Register Screen11 Scenario1");
	
	driver.findElement(By.xpath("//button[text()='Continue']")).click();
	Thread.sleep(3000);
}

@Then("Click Menu button for menu screen Scenario5")
public void click_menu_button_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
	Thread.sleep(2000);
}
@Then("Click My Profile for menu screen Scenario5")
public void click_my_profile_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='My Profile']")).click();
	Thread.sleep(2000);
}
@Then("Click My Athletic Profile for menu screen Scenario5")
public void click_my_athletic_profile_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='My Athletic Profile']")).click();
	Thread.sleep(3000);
}
@Then("Click the back arrow for menu screen Scenario5")
public void click_the_back_arrow_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='header-btns']")).click();
	Thread.sleep(3000);
}
@Then("Click the My Kids for menu screen Scenario5")
public void click_the_my_kids_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='My Kids']")).click();
	Thread.sleep(3000);
}
@Then("Click  Add more kids button for menu screen Scenario5")
public void click_add_more_kids_button_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='addkids-button']")).click();
	Thread.sleep(3000);
}
@Then("Enter the firstname for menu screen Scenario5")
public void enter_the_firstname_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Hameed");
	Thread.sleep(3000);
}
@Then("Enter the lastname for menu screen Scenario5")
public void enter_the_lastname_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Rameez");
	Thread.sleep(3000);
}
@Then("Choose the DOB for menu screen Scenario5")
public void choose_the_dob_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//img[@height='16']")).click();
	Thread.sleep(3000);
}
@Then("Choose the DOBYear for menu screen Scenario5")
public void choose_the_dob_year_for_menu_screen_scenario5() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]")).click();
	Thread.sleep(3000);
}
@Then("Choose the DOBMonth for menu screen Scenario5")
public void choose_the_dob_month_for_menu_screen_scenario5() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
	Thread.sleep(3000);
}
@Then("Choose the DOBDate for menu screen Scenario5")
public void choose_the_dob_date_for_menu_screen_scenario5() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[25]"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
	Thread.sleep(3000);
}
@Then("click the Confirm Date button for menu screen Scenario5")
public void click_the_confirm_date_button_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(3000);
}
@Then("click the gender for menu screen Scenario5")
public void click_the_gender_for_menu_screen_scenario5() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Male']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	//driver.findElement(By.xpath("//p[text()='Male']")).click();
	Thread.sleep(3000);
}
@Then("Slide the Height for menu screen Scenario5")
public void slide_the_height_for_menu_screen_scenario5() throws InterruptedException {
	WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
	
	for (int i = 0; i <= 130 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
	}
}
@Then("Slide the Weight for menu screen Scenario5")
public void slide_the_weight_for_menu_screen_scenario5() throws InterruptedException {
	WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
	
	for (int i = 0; i <= 25 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
	}
}
@Then("Slide the Shoe Size for menu screen Scenario5")
public void slide_the_shoe_size_for_menu_screen_scenario5() throws InterruptedException {
	WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
	
	for (int i = 24; i <= 31 ; i++) {
        slider.sendKeys(Keys.ARROW_RIGHT);
	}
}
@Then("Click Add Kids for menu screen Scenario5")
public void click_add_kids_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Add Kids']")).click();
	Thread.sleep(3000);
}
@Then("Click Transportation Preferences for menu screen Scenario5")
public void click_transportation_preferences_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Transportation Preferences']")).click();
	Thread.sleep(3000);
}
@Then("Click the back arrow transportation for menu screen Scenario5")
public void click_the_back_arrow_transportation_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//img[@height='24']")).click();
	Thread.sleep(3000);
}
@Then("Click Accommodation Preferences for menu screen Scenario5")
public void click_accommodation_preferences_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Accommodation Preferences']")).click();
	Thread.sleep(3000);
}
@Then("Click the back arrow accommodation for menu screen Scenario5")
public void click_the_back_arrow_accommodation_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//img[@height='24']")).click();
	Thread.sleep(3000);
}
@Then("Click Preferred Locations for menu screen Scenario5")
public void click_preferred_locations_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Preferred Locations']")).click();
	Thread.sleep(3000);
}
@Then("Click the back arrow location for menu screen Scenario5")
public void click_the_back_arrow_location_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//img[@height='24']")).click();
	Thread.sleep(3000);
}
@Then("Click Communication Preferences for menu screen Scenario5")
public void click_communication_preferences_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Communication Preferences']")).click();
	Thread.sleep(3000);
}
@Then("Click the back arrow communication for menu screen Scenario5")
public void click_the_back_arrow_communication_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//img[@height='24']")).click();
	Thread.sleep(3000);
}
@Then("Click the Menu to go Home for booking for menu screen Scenario5")
public void click_the_menu_to_go_home_for_booking_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
	Thread.sleep(3000);
}
@Then("click the Home button for menu screen Scenario5")
public void click_the_home_button_for_menu_screen_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Home']")).click();
	Thread.sleep(3000);
}

@Then("Click Plan a trip button for Screen12 Scenario5")
public void click_plan_a_trip_button_for_screen12_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}

@Then("Select first location checkbox for Screen13 Scenario5")
public void select_first_location_checkbox_for_screen13_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Chäserrugg']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen13 Scenario5")
public void click_next_button_for_screen13_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("Click on start date button for Screen14 Scenario5")
public void click_on_start_date_button_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Select start date for Screen14 Scenario5")
public void select_start_date_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 20, 2021']")).click();
	Thread.sleep(2000);
}
@Then("select End date for Screen14 Scenario5")
public void select_end_date_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 22, 2021']")).click();
	Thread.sleep(2000);
}
@Then("Click start time dropdown for Screen14 Scenario5")
public void click_start_time_dropdown_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
	Thread.sleep(2000);
	
}
@Then("select start time for Screen14 Scenario5")
public void select_start_time_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Click End time dropdown for Screen14 Scenario5")
public void click_end_time_dropdown_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
	Thread.sleep(2000);
}
@Then("select end timefor Screen14 Scenario5")
public void select_end_timefor_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
	Thread.sleep(2000);
}
@Then("Click confirm button for Screen14 Scenario5")
public void click_confirm_button_for_screen14_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen14 Scenario5")
public void click_next_button_for_screen14_scenario15() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}

@Then("Select first equipment for Screen15 Scenario5")
public void select_first_equipment_for_screen15_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski']")).click();
	Thread.sleep(2000);
}
/*@Then("Select second equipment for Screen15 Scenario1")
public void select_second_equipment_for_screen15_scenario1() {
	driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
}
@Then("Select third equipment for Screen15 Scenario1")
public void select_third_equipment_for_screen15_scenario1() {
	driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
}
@Then("Select fourth equipment for Screen15 Scenario1")
public void select_fourth_equipment_for_screen15_scenario1() {
	driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
}*/
@Then("Click Yes button for Screen15 Scenario5")
public void click_yes_button_for_screen15_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='next button-style']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for Screen16 Scenario5")
public void click_next_button_for_screen16_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Next Step']")).click();
	Thread.sleep(2000);
}

@Then("Click checkbox for Screen17 Scenario5")
public void click_checkbox_for_screen17_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//p[contains(text(),'Send me a link to')]")).click();
	Thread.sleep(2000);
}

@Then("Click yes button for Screen17 Scenario5")
public void click_yes_button_for_screen17_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()=' Next Step ']")).click();
	Thread.sleep(2000);
}

@Then("Click Payment button for Screen18 Scenario5")
public void click_payment_button_for_screen18_scenario5() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Continue to Payment']")).click();
	Thread.sleep(5000);
	driver.close();
}
/////////////////////////////////////////////////////////////////////////////////

	//@Booking1 @negative @all

@Given("open booking url")
public void open_booking_url() {
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
}
@Then("click on plan a trip for negative scenario1")
public void click_on_plan_a_trip_for_negative_scenario1() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}
@Then("click on next for negative scenario1")
public void click_on_next_for_negative_scenario1() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
	driver.close();
}


@Given("open booking url for negative scenario2")
public void open_booking_url_for_negative_scenario2() {
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
}
@Then("click on plan a trip for negative scenario2")
public void click_on_plan_a_trip_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}
@Then("select location for negative scenario2")
public void select_location_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Braunwald']")).click();
	Thread.sleep(2000);
}
@Then("click next for negative scenario2")
public void click_next_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}
@Then("Click on start date button for negative scenario2")
public void click_on_start_date_button_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Select start date for negative scenario2")
public void select_start_date_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 23, 2021']")).click();
	Thread.sleep(2000);
}
@Then("select End date for negative scenario2")
public void select_end_date_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='October 24, 2021']")).click();
	Thread.sleep(2000);
}
@Then("Click start time dropdown for negative scenario2")
public void click_start_time_dropdown_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
	Thread.sleep(2000);
}
@Then("select start time for negative scenario2")
public void select_start_time_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Click End time dropdown for negative scenario2")
public void click_end_time_dropdown_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
	Thread.sleep(2000);
}
@Then("select end time for negative scenario2")
public void select_end_time_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
	Thread.sleep(2000);
}
@Then("Click confirm button for negative scenario2")
public void click_confirm_button_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for negative scenario2")
public void click_next_button_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Next']")).click();
	Thread.sleep(2000);
}
@Then("click skip button for negative scenario2")
public void click_skip_button_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='skip button-style']")).click();
	Thread.sleep(2000);
}
@Then("click next step for for negative scenario2")
public void click_next_step_for_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Next Step']")).click();
	Thread.sleep(2000);
}
@Then("click on checkbox for negative scenario2")
public void click_on_checkbox_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//p[contains(text(),'Send me a link to')]")).click();
	Thread.sleep(2000);
}
@Then("Click on nextstep for negative scenario2")
public void click_on_nextstep_for_negative_scenario2() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()=' Next Step ']")).click();
	Thread.sleep(4000);
	driver.close();
}


	//@Booking4 @message @all

@Given("open booking url for negative scenario3")
public void open_booking_url_for_negative_scenario3() {
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
}
@Then("click on plan a trip for negative scenario3")
public void click_on_plan_a_trip_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}
@Then("select location for negative scenario3")
public void select_location_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Pizol']")).click();
	Thread.sleep(2000);
}
@Then("click next for negative scenario3")
public void click_next_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Weiter']")).click();
	Thread.sleep(2000);
}
@Then("Click on start date button for negative scenario3")
public void click_on_start_date_button_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Select start date for negative scenario3")
public void select_start_date_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='31. Oktober 2021']")).click();
	Thread.sleep(2000);
}
@Then("select End date for negative scenario3")
public void select_end_date_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//td[@aria-label='31. Oktober 2021']")).click();
	Thread.sleep(2000);
}
@Then("Click start time dropdown for negative scenario3")
public void click_start_time_dropdown_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
	Thread.sleep(2000);
}
@Then("select start time for negative scenario3")
public void select_start_time_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Click End time dropdown for negative scenario3")
public void click_end_time_dropdown_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
	Thread.sleep(2000);
}
@Then("select end time for negative scenario3")
public void select_end_time_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
	Thread.sleep(2000);
}
@Then("Click confirm button for negative scenario3")
public void click_confirm_button_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(2000);
}
@Then("Click next button for negative scenario3")
public void click_next_button_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()='Weiter']")).click();
	Thread.sleep(2000);
}
@Then("select first equipment for negative scenario3")
public void select_first_equipment_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski']")).click();
	Thread.sleep(2000);
}
@Then("select second equipment for negative scenario3")
public void select_second_equipment_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
	Thread.sleep(2000);
}
@Then("select third equipment for negative scenario3")
public void select_third_equipment_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski Jacket']")).click();
	Thread.sleep(2000);
}
@Then("select fourth equipment for negative scenario3")
public void select_fourth_equipment_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//div[text()='Ski Pant']")).click();
	Thread.sleep(2000);
}
@Then("select fifth equipment for negative scenario3")
public void select_fifth_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Poles']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select sixth equipment for negative scenario3")
public void select_sixth_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Gloves']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select seventh equipment for negative scenario3")
public void select_seventh_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Helmet']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select eighth equipment for negative scenario3")
public void select_eighth_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Goggle']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select nineth equipment for negative scenario3")
public void select_nineth_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select tenth equipment for negative scenario3")
public void select_tenth_equipment_for_negative_scenario3() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard Boot']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("click skip button for negative scenario3")
public void click_skip_button_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//button[@class='skip button-style']")).click();
	Thread.sleep(2000);
}
@Then("click next step for for negative scenario3")
public void click_next_step_for_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//span[text()='Next Step']")).click();
	Thread.sleep(2000);
}
@Then("click on checkbox for negative scenario3")
public void click_on_checkbox_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//p[contains(text(),'Send me a link to')]")).click();
	Thread.sleep(2000);
}
@Then("Click on nextstep for negative scenario3")
public void click_on_nextstep_for_negative_scenario3() throws InterruptedException {
	driver.findElement(By.xpath("//button[text()=' Next Step ']")).click();
	Thread.sleep(2000);
	//driver.close();
}

////////////////////////////////////////////////////////////////////////////////

		

@Given("open booking url for message scenario4")
public void open_booking_url_for_message_scenario4() {
    System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
	
	ChromeOptions options = new ChromeOptions();
	
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	//options.addArguments("headless");
	//options.addArguments("disable-gpu");
	
	driver = new ChromeDriver(options);
	driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
}
@Then("click on plan a trip for message scenario4")
public void click_on_plan_a_trip_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
	Thread.sleep(2000);
}
@Then("select location for message scenario4")
public void select_location_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//p[text()='Pizol']")).click();
	Thread.sleep(2000);
}
@Then("click next for message scenario4")
public void click_next_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//button[text()='Weiter']")).click();
	Thread.sleep(2000);
}
@Then("Click on start date button for message scenario4")
public void click_on_start_date_button_for_message_scenario4() throws InterruptedException {
  driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Select start date for message scenario4")
public void select_start_date_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//td[@aria-label='31. Oktober 2021']")).click();
	Thread.sleep(2000);
}
@Then("select End date for message scenario4")
public void select_end_date_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//td[@aria-label='31. Oktober 2021']")).click();
	Thread.sleep(2000);
}
@Then("Click start time dropdown for message scenario4")
public void click_start_time_dropdown_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
	Thread.sleep(2000);
}
@Then("select start time for message scenario4")
public void select_start_time_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
	Thread.sleep(2000);
}
@Then("Click End time dropdown for message scenario4")
public void click_end_time_dropdown_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
	Thread.sleep(2000);
}
@Then("select end time for message scenario4")
public void select_end_time_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
	Thread.sleep(2000);
}
@Then("Click confirm button for message scenario4")
public void click_confirm_button_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//span[text()='Confirm Date']")).click();
	Thread.sleep(2000);
}
@Then("Click next btn for message scenario4")
public void click_next_btn_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//button[text()='Weiter']")).click();
	Thread.sleep(2000);
}
@Then("select first equipment for message scenario4")
public void select_first_equipment_for_message_scenario4() throws InterruptedException {
 driver.findElement(By.xpath("//div[text()='Ski']")).click();
	Thread.sleep(2000);
}
@Then("select second equipment for message scenario4")
public void select_second_equipment_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//div[text()='Ski Boots']")).click();
	Thread.sleep(2000);
}
@Then("select third equipment for message scenario4")
public void select_third_equipment_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//div[text()='Ski Jacket']")).click();
	Thread.sleep(2000);
}
@Then("select fourth equipment for message scenario4")
public void select_fourth_equipment_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//div[text()='Ski Pant']")).click();
	Thread.sleep(2000);
}
@Then("select fifth equipment for message scenario4")
public void select_fifth_equipment_for_message_scenario4() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Ski Poles']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select sixth equipment for message scenario4")
public void select_sixth_equipment_for_message_scenario4() throws InterruptedException {
	WebDriverWait wait = new WebDriverWait(driver, 10);
   WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Gloves']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select seventh equipment for message scenario4")
public void select_seventh_equipment_for_message_scenario4() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Helmet']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select eighth equipment for message scenario4")
public void select_eighth_equipment_for_message_scenario4() throws InterruptedException {
   WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Goggle']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select nineth equipment for message scenario4")
public void select_nineth_equipment_for_message_scenario4() throws InterruptedException {
   WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("select tenth equipment for message scenario4")
public void select_tenth_equipment_for_message_scenario4() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard Boot']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("click on add message button for message scenario4")
public void click_on_add_message_button_for_message_scenario4() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Nachricht für den Verleih hinterlassen']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("click text box for message scenario4")
public void click_text_box_for_message_scenario4() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea[@id='message']"))); 
	((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
	Thread.sleep(2000);
}
@Then("Enter message for message scenario4")
public void enter_message_for_message_scenario4() {
	driver.findElement(By.xpath("//textarea[@name='message']")).sendKeys("No need any other help");
}
@Then("click next step button for message scenario4")
public void click_next_step_button_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//button[@class='next button-style']")).click();
	Thread.sleep(2000);
}
@Then("click next button for message scenario4")
public void click_next_button_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//span[text()='Weiter']")).click();
	Thread.sleep(2000);
}
@Then("click checkbox1 for message scenario4")
public void click_checkbox1_for_message_scenario4() throws InterruptedException {
   //p[text()='Ski-Pass Erwachsener, HTA ']
 driver.findElement(By.xpath("//p[text()='Ski-Pass Erwachsener, HTA ']")).click();
	Thread.sleep(2000);
}
@Then("click checkbox2 for message scenario4")
public void click_checkbox2_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Sende mir einen Link zum Onlineshop ']")).click();
	Thread.sleep(2000);
}
@Then("click add ski pass for message scenario4")
public void click_add_ski_pass_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()=' Ski-Pass kaufen ']")).click();
	Thread.sleep(2000);
}
@Then("click continue payment for message scenario4")
public void click_continue_payment_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter zur Bezahlung']")).click();
	Thread.sleep(3000);
}
@Then("Enter card number for message scenario4")
public void enter_card_number_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//input[@name='cardNumber']")).sendKeys("4242 4242 4242 4242");
		Thread.sleep(2000);
}
@Then("Enter card expiry for message scenario4")
public void enter_card_expiry_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//input[@name='cardExpiry']")).sendKeys("0624");
		Thread.sleep(2000);
}
@Then("Enter cvc for message scenario4")
public void enter_cvc_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//input[@name='cardCvc']")).sendKeys("987");
		Thread.sleep(2000);
}
@Then("Enter name for message scenario4")
public void enter_name_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//input[@name='billingName']")).sendKeys("Rameez");
		Thread.sleep(2000);
}
@Then("select country for message scenario4")
public void select_country_for_message_scenario4() throws InterruptedException {
   Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='billingCountry']")));
		drpCountry.selectByVisibleText("India");
		Thread.sleep(2000);
}
@Then("click pay button for message scenario4")
public void click_pay_button_for_message_scenario4() throws InterruptedException {
    driver.findElement(By.xpath("//div[@class='SubmitButton-IconContainer']")).click();
		Thread.sleep(4000);
}
@Then("click back to home button for message scenario4")
public void click_back_to_home_button_for_message_scenario4() throws InterruptedException {
   driver.findElement(By.xpath("//button[text()='Zurück zur Startseite']")).click();
		Thread.sleep(4000);
		driver.close();
}

////////////////////////////////////////////////////////////////////////////////

//@Dlfl @Registerdl

@Given("browser is open for Registerdl")
public void browser_is_open_for_registerdl() {
    System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
		ChromeOptions options = new ChromeOptions();
		
		//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
		
		//options.addArguments("headless");
		//options.addArguments("disable-gpu");
		
		driver = new ChromeDriver(options);
		driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/register");
		
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
}
@Then("Enter the firstname for Registerdl")
public void enter_the_firstname_for_registerdl() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rameez");
		Thread.sleep(2000);
}
@Then("Enter the lastname for Registerdl")
public void enter_the_lastname_for_registerdl() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Hameed");	
		Thread.sleep(2000);
}
@Then("Enter the email field for Registerdl")
public void enter_the_email_field_for_registerdl() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("alpinist1252@gmail.com");
		Thread.sleep(2000);
}
@Then("Choose the DOB for Registerdl")
public void choose_the_dob_for_registerdl() {
   driver.findElement(By.xpath("//div[@class='single-date-picker flex align-center']")).click();
}
@Then("Choose the DOBYear for Registerdl")
public void choose_the_dob_year_for_registerdl() {
    driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[15]")).click();
}
@Then("Choose the DOBMonth for Registerdl")
public void choose_the_dob_month_for_registerdl() {
    driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[2]")).click();
}
@Then("Choose the DOBDate for Registerdl")
public void choose_the_dob_date_for_registerdl() {
    driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-content mat-focus-indicator'])[20]")).click();
}
@Then("click the Confirm Date button for Registerdl")
public void click_the_confirm_date_button_for_registerdl() {
   driver.findElement(By.xpath("//span[contains(text(),'Bestätigen')]")).click();
}
@Then("Enter and enter the password for Registerdl")
public void enter_and_enter_the_password_for_registerdl() {
   driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("XXyyzz_11");
}
@Then("Enter and enter the confirm password for Registerdl")
public void enter_and_enter_the_confirm_password_for_registerdl() {
   driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("XXyyzz_11");
}
@Then("click the Continue button for Registerdl")
public void click_the_continue_button_for_registerdl() throws InterruptedException {
   driver.findElement(By.xpath("//button[contains(text(),'Weiter')]")).click();
		Thread.sleep(2000);
}

@Then("click the Next button for Registerdl Screen2")
public void click_the_next_button_for_registerdl_screen2() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("click first location for Registerdl Screen3")
public void click_first_location_for_registerdl_screen3() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='check-tick-icon'])[2]")).click();
		Thread.sleep(2000);
}
@Then("click the Next button for Registerdl Screen3")
public void click_the_next_button_for_registerdl_screen3() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("click the gender for Registerdl Screen4")
public void click_the_gender_for_registerdl_screen4() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Slide the Height for Registerdl Screen4")
public void slide_the_height_for_registerdl_screen4() throws InterruptedException {
   WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i <= 90 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
}
@Then("Slide the Weight for Registerdl Screen4")
public void slide_the_weight_for_registerdl_screen4() throws InterruptedException {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i <= 50 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
}
@Then("Slide the Shoe Size for Registerdl Screen4")
public void slide_the_shoe_size_for_registerdl_screen4() throws InterruptedException {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 24; i <= 30 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);	
}
@Then("click the Continue button for Registerdl Screen4")
public void click_the_continue_button_for_registerdl_screen4() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		
		Thread.sleep(2000);
}

@Then("click the Skill type for Registerdl Screen5")
public void click_the_skill_type_for_registerdl_screen5() throws InterruptedException {
		driver.findElement(By.xpath("(//div[@class='custom-option-btn width-50'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Slide the Select Skill for Registerdl Screen5")
public void slide_the_select_skill_for_registerdl_screen5() throws InterruptedException {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='4']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);
}
@Then("Slide the Select Gear grade for Registerdl Screen5")
public void slide_the_select_gear_grade_for_registerdl_screen5() throws InterruptedException {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		
		Thread.sleep(2000);
}
@Then("click the Continue button for Registerdl Screen5")
public void click_the_continue_button_for_registerdl_screen5() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("click the Transport type for Registerdl Screen6")
public void click_the_transport_type_for_registerdl_screen6() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-checkbox width-50'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Enter the number plate value for Registerdl Screen6")
public void enter_the_number_plate_value_for_registerdl_screen6() throws InterruptedException {
   driver.findElement(By.xpath("//input[@name='license']")).sendKeys("TN38AA");
		Thread.sleep(2000);
}
@Then("click the Next button for Registerdl Screen6")
public void click_the_next_button_for_registerdl_screen6() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
}

@Then("Slide the Accomodation for Registerdl Screen7")
public void slide_the_accomodation_for_registerdl_screen7() throws InterruptedException {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='3']"));
		
		for (int i = 1; i <= 2 ; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
		Thread.sleep(2000);
}

@Then("click the Next button for Registerdl Screen7")
public void click_the_next_button_for_registerdl_screen7() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
}

@Then("Delete firstname for Registerdl Screen8")
public void delete_firstname_for_registerdl_screen8() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='FirstName']")).clear();
		Thread.sleep(2000);
}

@Then("Delete lastname for Registerdl Screen8")
public void delete_lastname_for_registerdl_screen8() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='LastName']")).clear();
		Thread.sleep(2000);
}

@Then("Enter the Street for Registerdl Screen8")
public void enter_the_street_for_registerdl_screen8() throws InterruptedException {
   driver.findElement(By.xpath("//input[@id='street']")).sendKeys("Pammal");
		Thread.sleep(2000);
}
@Then("Enter the Number for Registerdl Screen8")
public void enter_the_number_for_registerdl_screen8() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='number']")).sendKeys("135");
		Thread.sleep(2000);
}
@Then("Enter the Zip code for Registerdl Screen8")
public void enter_the_zip_code_for_registerdl_screen8() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("640007");
		Thread.sleep(2000);
}
@Then("Enter the City for Registerdl Screen8")
public void enter_the_city_for_registerdl_screen8() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Chennai");
		Thread.sleep(2000);
}
@Then("Choose the Country for Registerdl Screen8")
public void choose_the_country_for_registerdl_screen8() throws InterruptedException {
   Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='countrySelect']")));
		drpCountry.selectByVisibleText("🇮🇳 Indien");
		Thread.sleep(2000);
}
@Then("click the Continue button for Registerdl Screen8")
public void click_the_continue_button_for_registerdl_screen8() throws InterruptedException {
   driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("Click the next button for Registerdl Screen9")
public void click_the_next_button_for_registerdl_screen9() throws InterruptedException {
    driver.findElement(By.xpath("//button[contains(text(),' Weiter ')]")).click();
		Thread.sleep(2000);
}

@Then("Click the first checkboxes for Registerdl Screen10")
public void click_the_first_checkboxes_for_registerdl_screen10() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='checkbox-text'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Click the second checkboxes for Registerdl Screen10")
public void click_the_second_checkboxes_for_registerdl_screen10() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='checkbox-text'])[2]")).click();
		Thread.sleep(2000);
}
@Then("click the Complete Sign Up button for Registerdl Screen10")
public void click_the_complete_sign_up_button_for_registerdl_screen10() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Anmeldung abschliessen']")).click();
		Thread.sleep(2000);
}

@Then("Click the promocode checkbox for Registerdl Screen11")
public void click_the_promocode_checkbox_for_registerdl_screen11() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Ich habe einen Promo Code']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
}
@Then("Click promocode for Registerdl Screen11")
public void click_promocode_for_registerdl_screen11() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='promo']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Thread.sleep(2000);
}
@Then("Enter promocode for Registerdl Screen11")
public void enter_promocode_for_registerdl_screen11() throws InterruptedException {
   driver.findElement(By.xpath("//input[@name='promo']")).sendKeys("1111122222");
		Thread.sleep(2000);
}
@Then("click the Continue button for Registerdl Screen11")
public void click_the_continue_button_for_registerdl_screen11() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(3000);
}

///////////////////////////////////////////////////////////////////////////

//@testing

@Given("open testing url")
public void open_testing_url() {
    		
	System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
		
	ChromeOptions options = new ChromeOptions();
	
	//options.addArguments("enable-automation");
	//options.addArguments("--headless");
	//options.addArguments("--window-size=1920,1080");
	//options.addArguments("--no-sandbox");
	//options.addArguments("--disable-extensions");
	//options.addArguments("--dns-prefetch-disable");
	//options.addArguments("--disable-gpu");
	//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	
	driver = new ChromeDriver(options);
	driver.get("https://devpro1-alpinistcrm.cs173.force.com/#/login?username=abdulkadher2307@gmail.com&pwd=Abdul@123");
	
	driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		
		//driver.manage().window().maximize();
}
@Then("Click Menu button for testing")
public void click_menu_button_for_testing() throws InterruptedException {
    		driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
		Thread.sleep(2000);
}
@Then("Click My Profile for testing")
public void click_my_profile_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Mein Profil']")).click();
		Thread.sleep(2000);
}
@Then("Click My Athletic Profile for testing")
public void click_my_athletic_profile_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Mein Körperprofil']")).click();
		Thread.sleep(3000);
}
@Then("Click the back arrow for testing")
public void click_the_back_arrow_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[@class='header-btns']")).click();
		Thread.sleep(3000);
}
@Then("Click the My Kids for testing")
public void click_the_my_kids_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Meine Kinder']")).click();
		Thread.sleep(3000);
}
@Then("Click  Add more kids button for testing")
public void click_add_more_kids_button_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[@class='addkids-button']")).click();
		Thread.sleep(3000);
}
@Then("Enter the firstname for testing")
public void enter_the_firstname_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Hameed");
		Thread.sleep(3000);
}
@Then("Enter the lastname for testing")
public void enter_the_lastname_for_testing() throws InterruptedException {
   driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Rameez");
		Thread.sleep(3000);
}
@Then("Choose the DOB for testing")
public void choose_the_dob_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//img[@height='16']")).click();
		Thread.sleep(3000);
}
@Then("Choose the DOBYear for testing")
public void choose_the_dob_year_for_testing() throws InterruptedException {
   WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[18]")).click();
		Thread.sleep(3000);
}
@Then("Choose the DOBMonth for testing")
public void choose_the_dob_month_for_testing() throws InterruptedException {
   WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
		Thread.sleep(3000);
}
@Then("Choose the DOBDate for testing")
public void choose_the_dob_date_for_testing() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[25]"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("(//div[@class='mat-calendar-body-cell-preview'])[6]")).click();
		Thread.sleep(3000);
}
@Then("click the Confirm Date testing")
public void click_the_confirm_date_testing() throws InterruptedException {
   driver.findElement(By.xpath("//span[text()='Bestätigen']")).click();
		Thread.sleep(3000);
}
@Then("click the gender for testing")
public void click_the_gender_for_testing() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Männlich']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//p[text()='Male']")).click();
		Thread.sleep(3000);
}
@Then("Slide the Height for testing")
public void slide_the_height_for_testing() {
    WebElement slider = driver.findElement(By.xpath("(//span[@class='ngx-slider-span ngx-slider-pointer ngx-slider-pointer-min'])[1]"));
		
		for (int i = 0; i < 80 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
}
@Then("Slide the Weight for testing")
public void slide_the_weight_for_testing() {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='150']"));
		
		for (int i = 0; i < 30 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
}
@Then("Slide the Shoe Size for testing")
public void slide_the_shoe_size_for_testing() {
    WebElement slider = driver.findElement(By.xpath("//span[@aria-valuemax='51']"));
		
		for (int i = 12; i <= 20 ; i++) {
	        slider.sendKeys(Keys.ARROW_RIGHT);
		}
}
@Then("Click Add Kids for testing")
public void click_add_kids_for_testing() throws InterruptedException {
   driver.findElement(By.xpath("//span[text()=' Kinder hinzufügen']")).click();
		Thread.sleep(3000);
}
@Then("click the back arrow add kids for testing")
public void click_the_back_arrow_add_kids_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//img[@width='24']")).click();
		Thread.sleep(2000);
}
@Then("Click Transportation Preferences for testing")
public void click_transportation_preferences_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Bevorzugte Transportmittel']")).click();
		Thread.sleep(3000);
}
@Then("Click the back arrow transportation for testing")
public void click_the_back_arrow_transportation_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
}
@Then("Click Accommodation Preferences for testing")
public void click_accommodation_preferences_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Bevorzugte Unterkunft']")).click();
		Thread.sleep(3000);	
}
@Then("Click the back arrow accommodation for testing")
public void click_the_back_arrow_accommodation_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
}
@Then("Click Preferred Locations for testing")
public void click_preferred_locations_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Bevorzugte Skigebiete']")).click();
		Thread.sleep(3000);
}
@Then("Click the back arrow location for testing")
public void click_the_back_arrow_location_for_testing() throws InterruptedException {
    		driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
}
@Then("Click Communication Preferences for testing")
public void click_communication_preferences_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Bevorzugte Kommunikationsart']")).click();
		Thread.sleep(3000);
}
@Then("Click the back arrow communication for testing")
public void click_the_back_arrow_communication_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//img[@height='24']")).click();
		Thread.sleep(3000);
}
@Then("Click the Menu to go Home for booking for testing")
public void click_the_menu_to_go_home_for_booking_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//div[@class='header-btns hamburger-style']")).click();
		Thread.sleep(3000);
}
@Then("click the Home button for testing")
public void click_the_home_button_for_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Home']")).click();
		Thread.sleep(3000);
}

@Then("Click Plan a trip button for testing Screen12")
public void click_plan_a_trip_button_for_testing_screen12() throws InterruptedException {
    driver.findElement(By.xpath("//div[@class='alpinist-btn']")).click();
		Thread.sleep(2000);
}

@Then("Select first location checkbox for testing Screen13")
public void select_first_location_checkbox_for_testing_screen13() throws InterruptedException {
    driver.findElement(By.xpath("//p[text()='Braunwald']")).click();
		Thread.sleep(2000);
}
@Then("Click next button for testing Screen13")
public void click_next_button_for_testing_screen13() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("Click on start date button for testing Screen14")
public void click_on_start_date_button_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("(//span[@class='ng-star-inserted'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Select start date for testing Screen14")
public void select_start_date_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("//td[@aria-label='15. November 2021']")).click();
		Thread.sleep(2000);
}
@Then("select End date for testing Screen14")
public void select_end_date_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("//td[@aria-label='16. November 2021']")).click();
		Thread.sleep(2000);
}
@Then("Click start time dropdown for testing Screen14")
public void click_start_time_dropdown_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[1]")).click();
		Thread.sleep(2000);
}
@Then("select start time for testing Screen14")
public void select_start_time_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("(//option[@value='9:00'])[1]")).click();
		Thread.sleep(2000);
}
@Then("Click End time dropdown for testing Screen14")
public void click_end_time_dropdown_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("(//div[@class='custom-select time-dropdown'])[2]")).click();
		Thread.sleep(2000);
}
@Then("select end time for testing Screen14")
public void select_end_time_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("(//option[@value='17:00'])[2]")).click();
		Thread.sleep(2000);
}
@Then("Click confirm button for testing Screen14")
public void click_confirm_button_for_testing_screen14() throws InterruptedException {
    driver.findElement(By.xpath("//span[text()='Bestätigen']")).click();
		Thread.sleep(2000);
}
@Then("Click next button for testing Screen14")
public void click_next_button_for_testing_screen14() throws InterruptedException {
   driver.findElement(By.xpath("//button[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("Select first equipment for Screen15 testing")
public void select_first_equipment_for_screen15_testing() throws InterruptedException {
    driver.findElement(By.xpath("//div[text()='Ski']")).click();
		Thread.sleep(2000);
}
@Then("Select second equipment for Screen15 testing")
public void select_second_equipment_for_screen15_testing() {
    driver.findElement(By.xpath("//div[text()='Skischuhe']")).click();
}
@Then("Select third equipment for Screen15 testing")
public void select_third_equipment_for_screen15_testing() {
    driver.findElement(By.xpath("//div[text()='Skijacke']")).click();
}
@Then("Select fourth equipment for Screen15 testing")
public void select_fourth_equipment_for_screen15_testing() {
    driver.findElement(By.xpath("//div[text()='Skihose']")).click();
}
@Then("Select fifth equipment for Screen15 testing")
public void select_fifth_equipment_for_screen15_testing() {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Skistöcke']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Ski Poles']")).click();
}
@Then("Select sixth equipment for Screen15 testing")
public void select_sixth_equipment_for_screen15_testing() {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Helm']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Helmet']")).click();
}
@Then("Select seventh equipment for Screen15 testing")
public void select_seventh_equipment_for_screen15_testing() {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboard']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Snowboard']")).click();
}
@Then("Select eighth equipment for Screen15 testing")
public void select_eighth_equipment_for_screen15_testing() {
    WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Snowboardschuhe']"))); 
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		//driver.findElement(By.xpath("//div[text()='Snowboard Boot']")).click();
}
@Then("Click Yes button for Screen15 testing")
public void click_yes_button_for_screen15_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()=' Ja, Buchung hinzufügen ']")).click();
		Thread.sleep(2000);
}

@Then("Click next button for Screen16 testing")
public void click_next_button_for_screen16_testing() throws InterruptedException {
    driver.findElement(By.xpath("//span[text()='Weiter']")).click();
		Thread.sleep(2000);
}

@Then("Click checkbox for Screen17 testing")
public void click_checkbox_for_screen17_testing() throws InterruptedException {
    driver.findElement(By.xpath("//p[contains(text(),'Ich möchte nach Buchungsabschluss')]")).click();
		Thread.sleep(2000);
}
@Then("Click yes button for Screen17 testing")
public void click_yes_button_for_screen17_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()=' Weiter ']")).click();
		Thread.sleep(2000);
}

@Then("Click Payment button for Screen18 testing")
public void click_payment_button_for_screen18_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Weiter zur Bezahlung']")).click();
		Thread.sleep(2000);
		driver.close();
}

/*@Then("Enter card number for Screen19 testing")
public void enter_card_number_for_screen19_testing() throws InterruptedException {
    driver.findElement(By.xpath("//input[@name='cardNumber']")).sendKeys("4242 4242 4242 4242");
		Thread.sleep(2000);
}
@Then("Enter expiry for Screen19 testing")
public void enter_expiry_for_screen19_testing() throws InterruptedException {
   driver.findElement(By.xpath("//input[@name='cardExpiry']")).sendKeys("0624");
		Thread.sleep(2000);
}
@Then("Enter ccv number for Screen19 testing")
public void enter_ccv_number_for_screen19_testing() throws InterruptedException {
    driver.findElement(By.xpath("//input[@name='cardCvc']")).sendKeys("987");
		Thread.sleep(2000);
}
@Then("Enter name on card for Screen19 testing")
public void enter_name_on_card_for_screen19_testing() throws InterruptedException {
    driver.findElement(By.xpath("//input[@name='billingName']")).sendKeys("Rameez");
		Thread.sleep(2000);
}
@Then("select region for Screen19 testing")
public void select_region_for_screen19_testing() throws InterruptedException {
    Select drpCountry = new Select(driver.findElement(By.xpath("//select[@id='billingCountry']")));
		drpCountry.selectByVisibleText("India");
		Thread.sleep(2000);
}
@Then("click on pay button for Screen19 testing")
public void click_on_pay_button_for_screen19_testing() throws InterruptedException {
    driver.findElement(By.xpath("//div[@class='SubmitButton-IconContainer']")).click();
		Thread.sleep(4000);
}
@Then("click on back to home for Screen19 testing")
public void click_on_back_to_home_for_screen19_testing() throws InterruptedException {
    driver.findElement(By.xpath("//button[text()='Zurück zur Startseite']")).click();
		Thread.sleep(2000);
		driver.close();
}*/

}
