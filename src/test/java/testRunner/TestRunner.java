package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="/var/lib/jenkins/workspace/alptestproject/src/test/resources/Features",
glue={"StepDefinitions"},
plugin = {"pretty", "html:target/reports/report.html"},
tags ="@testing")


public class TestRunner {

}

