Feature: Alpinist WebApp Automation

#Scenario: Without swiss pass ID, first location, Gender male, with decimal shoe size, Transport car, with promocode, first equipment
@ALP001 @Register @all @sc1
Scenario: Registration Scenario1
Given browser is open
Then Enter the firstname for Register Scenario1
Then Enter the lastname for Register Scenario1
Then Enter the email field for Register Scenario1
Then Choose the DOB for Register Scenario1
Then Choose the DOBYear for Register Scenario1
Then Choose the DOBMonth for Register Scenario1
Then Choose the DOBDate for Register Scenario1
Then click the Confirm Date button for Register Scenario1
Then Enter and enter the password for Register Scenario1
Then Enter and enter the confirm password for Register Scenario1
Then click the Continue button for Register Scenario1



@ALP10001 @Register @Screen2 @all @sc1
Scenario: Registration Screen2 Scenario1
Then click the Next button for Register Screen2 Scenario1


@ALP20001 @Register @Screen3 @all @sc1
Scenario: Registration Screen3 Scenario1
Then click first location for Register Screen3 Scenario1
Then click the Next button for Register Screen3 Scenario1


@ALP30001 @Register @Screen4 @all @sc1
Scenario: Registration Screen4 Scenario1
Then click the gender for Register Screen4 Scenario1
Then Slide the Height for Register Screen4 Scenario1
Then Slide the Weight for Register Screen4 Scenario1
Then Slide the Shoe Size for Register Screen4 Scenario1
#Then Enter the Ski boots size for Register Screen4 Scenario1
Then click the Continue button for Register Screen4 Scenario1


@ALP40001 @Register @Screen5 @all @sc1
Scenario: Registration Screen5 Scenario1
Then click the Skill type for Register Screen5 Scenario1
Then Slide the Select Skill for Register Screen5 Scenario1
Then Slide the Select Gear grade for Register Screen5 Scenario1
Then click the Continue button for Register Screen5 Scenario1


@ALP50001 @Register @Screen6 @all @sc1
Scenario: Registration Screen6 Scenario1
Then click the Transport type for Register Screen6 Scenario1
Then Enter the number plate value for Register Screen6 Scenario1
Then click the Next button for Register Screen6 Scenario1


@ALP60001 @Register @Screen7 @all @sc1
Scenario: Registration Screen7 Scenario1
Then Slide the Accomodation for Register Screen7 Scenario1
Then click the Next button for Register Screen7 Scenario1


@ALP70001 @Register @Screen8 @all @sc1
Scenario: Registration Screen8 Scenario1
Then Enter the Street for Register Screen8 Scenario1
Then Enter the Number for Register Screen8 Scenario1
Then Enter the Zip code for Register Screen8 Scenario1
Then Enter the City for Register Screen8 Scenario1
Then Choose the Country for Register Screen8 Scenario1
Then click the Continue button for Register Screen8 Scenario1


@ALP80001 @Register @Screen9 @all @sc1
Scenario: Registration Screen9 Scenario1
Then Click the next button for Register Screen9 Scenario1
#Then Click the skip button for Register Screen9 Scenario1


@ALP90001 @Register @Screen10 @all @sc1
Scenario: Registration Screen10 Scenario1
Then Click the first checkboxes for Register Screen10 Scenario1
Then Click the second checkboxes for Register Screen10 Scenario1
Then click the Complete Sign Up button for Register Screen10 Scenario1


@ALP11001 @Register @Screen11 @all @sc1
Scenario: Registration Screen11 Scenario
Then Click the promocode checkbox for Register Screen11 Scenario1
Then Click promocode for Register Screen11 Scenario1
Then Enter promocode for Register Screen11 Scenario1
Then click the Continue button for Register Screen11 Scenario1

@ALPM1001 @all @sc1
Scenario: Menu Scenario1
Then Click Menu button for menu screen Scenario1
Then Click My Profile for menu screen Scenario1
Then Click My Athletic Profile for menu screen Scenario1
Then Click the back arrow for menu screen Scenario1
Then Click the My Kids for menu screen Scenario1
Then Click  Add more kids button for menu screen Scenario1
Then Enter the firstname for menu screen Scenario1
Then Enter the lastname for menu screen Scenario1
Then Choose the DOB for menu screen Scenario1
Then Choose the DOBYear for menu screen Scenario1
Then Choose the DOBMonth for menu screen Scenario1
Then Choose the DOBDate for menu screen Scenario1
Then click the Confirm Date button for menu screen Scenario1
Then click the gender for menu screen Scenario1
Then Slide the Height for menu screen Scenario1
Then Slide the Weight for menu screen Scenario1
Then Slide the Shoe Size for menu screen Scenario1
Then Click Add Kids for menu screen Scenario1
Then Click Transportation Preferences for menu screen Scenario1
Then Click the back arrow transportation for menu screen Scenario1
Then Click Accommodation Preferences for menu screen Scenario1
Then Click the back arrow accommodation for menu screen Scenario1
Then Click Preferred Locations for menu screen Scenario1
Then Click the back arrow location for menu screen Scenario1
Then Click Communication Preferences for menu screen Scenario1
Then Click the back arrow communication for menu screen Scenario1
Then Click the Menu to go Home for booking for menu screen Scenario1
Then click the Home button for menu screen Scenario1

@ALP12001 @Booking @Screen12 @all @sc1
Scenario: Plan a trip Screen12 Scenario1
Then url
Then Click Plan a trip button for Screen12 Scenario1

@ALP13001 @Booking @Screen13 @all @sc1
Scenario: Plan a trip Screen13 Scenario1
Then Select first location checkbox for Screen13 Scenario1
Then Click next button for Screen13 Scenario1

@ALP14001 @Booking @Screen14 @all @sc1
Scenario: Plan a trip Screen14 Scenario1
Then Click on start date button for Screen14 Scenario1
Then Select start date for Screen14 Scenario1
Then select End date for Screen14 Scenario1
Then Click start time dropdown for Screen14 Scenario1
Then select start time for Screen14 Scenario1
Then Click End time dropdown for Screen14 Scenario1
Then select end timefor Screen14 Scenario1
Then Click confirm button for Screen14 Scenario1
Then Click next button for Screen14 Scenario1

@ALP15001 @Booking @Screen15 @all @sc1
Scenario: Plan a trip Screen15 Scenario1
Then Select first equipment for Screen15 Scenario1
Then Select second equipment for Screen15 Scenario1
Then Select third equipment for Screen15 Scenario1
Then Select fourth equipment for Screen15 Scenario1
Then Select fifth equipment for Screen15 Scenario1
Then Select sixth equipment for Screen15 Scenario1
Then Select seventh equipment for Screen15 Scenario1
Then Select eighth equipment for Screen15 Scenario1
Then Click Yes button for Screen15 Scenario1

@ALP16001 @Booking @Screen16 @all @sc1
Scenario: Plan a trip Screen16 Scenario1
Then Click next button for Screen16 Scenario1

@ALP17001 @Booking @Screen17 @all @sc1
Scenario: Plan a trip Screen17 Scenario1
Then Click checkbox for Screen17 Scenario1
Then Click yes button for Screen17 Scenario1

@ALP18001 @Booking @Screen18 @all @sc1
Scenario: Plan a trip Screen18 Scenario1
Then Click Payment button for Screen18 Scenario1

@ALP19001 @Booking @all @sc1
Scenario: Plan a trip Screen19 Scenario1
Then Enter card number for Screen19 Scenario1
Then Enter expiry for Screen19 Scenario1
Then Enter ccv number for Screen19 Scenario1
Then Enter name on card for Screen19 Scenario1
Then select region for Screen19 Scenario1
#Then click on save card checkbox for Screen19 Scenario1
#Then Enter mobile number for Screen19 Scenario1
Then click on pay button for Screen19 Scenario1
Then click on back to home for Screen19 Scenario1


Scenario: With swiss pass ID, second location, Gender female, decimal shoe size, Transport train, without promocode, second equipment
@ALP002 @Register2 @all @sc2
Scenario: Registration Scenario2
Given Open the browser
Then Click Enter Your Swiss Pass ID button for Register Scenario2
Then Click input swiss pass for Register Scenario2
Then Enter Swiss Pass Number for Register Scenario2
Then Click zip code for Register Scenario2
Then Enter ZIP Code of User for Register Scenario2
Then Click  Add Swiss Pass ID button for Register Scenario2
Then Click Allow button for Register Scenario2
Then click email field for Register Scenario2
Then Enter the email field + symbol for Register Scenario2
Then Choose the DOB for Register Scenario2
Then Choose the DOBYear for Register Scenario2
Then Choose the DOBMonth for Register Scenario2
Then Choose the DOBDate for Register Scenario2
Then click the Confirm Date button for Register Scenario2
Then Enter and enter the password for Register Scenario2
Then Enter and enter the confirm password for Register Scenario2
Then click the Continue button for Register Scenario2



@ALP10002 @Register2 @all @sc2
Scenario: Registration Screen2 Scenario2
Then click the Next button for Register Screen2 Scenario2


@ALP20002 @Register2 @all @sc2
Scenario: Registration Screen3 Scenario1
Then click second location for Register Screen3 Scenario2
Then click the Next button for Register Screen3 Scenario2


@ALP30002 @Register2 @all @sc2
Scenario: Registration Screen4 Scenario2
Then click the gender for Register Screen4 Scenario2
Then Slide the Height for Register Screen4 Scenario2
Then Slide the Weight for Register Screen4 Scenario2
Then Slide the Shoe Size for Register Screen4 Scenario2
#Then Enter the Ski boots size fo with decimal value for Register Screen4 Scenario2
Then click the Continue button for Register Screen4 Scenario2


@ALP40002 @Register2 @all @sc2
Scenario: Registration Screen5 Scenario1
Then click the Skill type for Register Screen5 Scenario2
Then Slide the Select Skill for Register Screen5 Scenario2
Then Slide the Select Gear grade for Register Screen5 Scenario2
Then click the Continue button for Register Screen5 Scenario2


@ALP50002 @Register2 @all @sc2
Scenario: Registration Screen6 Scenario2
Then click the Transport typeCfor Register Screen6 Scenario2
#Then Enter the number plate value for Register Screen6 Scenario2
Then click the Next button for Register Screen6 Scenario2


@ALP60002 @Register2 @all @sc2
Scenario: Registration Screen7 Scenario2
Then Slide the Accomodation for Register Screen7 Scenario2
Then click the Next button for Register Screen7 Scenario2


@ALP70002 @Register2 @all @sc2
Scenario: Registration Screen8 Scenario2
Then Enter the Street for Register Screen8 Scenario2
Then Enter the Number for Register Screen8 Scenario2
Then Enter the Zip code for Register Screen8 Scenario2
Then Enter the City for Register Screen8 Scenario2
Then Choose the Country for Register Screen8 Scenario2
Then click the Continue button for Register Screen8 Scenario2


@ALP80002 @Register2 @all @sc2
Scenario: Registration Screen9 Scenario2
Then Click the next button for Register Screen9 Scenario2
#Then Click the skip button for Register Screen9 Scenario1


@ALP90002 @Register2 @all @sc2
Scenario: Registration Screen10 Scenario2
Then Click the first checkboxes for Register Screen10 Scenario2
Then Click the second checkboxes for Register Screen10 Scenario2
Then click the Complete Sign Up button for Register Screen10 Scenario2


@ALP11002 @Register2


 @all @sc2
Scenario: Registration Screen11 Scenario2
Then click the Continue button for Register Screen11 Scenario2

@ALP12002 @all @sc2
Scenario: Plan a trip Screen12 Scenario2
Then Click Plan a trip button for Screen12 Scenario2

@ALP13002 @all @sc2
Scenario: Plan a trip Screen13 Scenario2
Then Select second location checkbox for Screen13 Scenario2
Then Click next button for Screen13 Scenario2

@ALP14002 @all @sc2
Scenario: Plan a trip Screen14 Scenario2
Then Click on start date button for Screen14 Scenario2
Then Select start date for Screen14 Scenario2
Then select End date for Screen14 Scenario2
Then Click start time dropdown for Screen14 Scenario2
Then select start time for Screen14 Scenario2
Then Click End time dropdown for Screen14 Scenario2
Then select end timefor Screen14 Scenario2
Then Click confirm button for Screen14 Scenario2
Then Click next button for Screen14 Scenario2

@ALP15002 @all @sc2
Scenario: Plan a trip Screen15 Scenario2
#Then Select first equipment for Screen15 Scenario2
Then Select second equipment for Screen15 Scenario2
Then Click Yes button for Screen15 Scenario2

@ALP16002 @all @sc2
Scenario: Plan a trip Screen16 Scenario2
Then Click next button for Screen16 Scenario2

@ALP17002 @all @sc2
Scenario: Plan a trip Screen17 Scenario2
Then Click checkbox for Screen17 Scenario2
Then Click yes button for Screen17 Scenario2

@ALP18002 @all @sc2
Scenario: Plan a trip Screen18 Scenario2
Then Click Payment button for Screen18 Scenario2


#Scenario3 With swiss pass ID, uncheck the second screen no record for sliding, third location, both car and train, uncheck the notifications, without poromocode,select all equipments, uncheck the last screen

@ALP003 @all @sc3
Scenario: Registration Scenario3
Given Browser to be open
Then Click Enter Your Swiss Pass ID button for Register Scenario3
Then Click input swiss pass for Register Scenario3
Then Enter Swiss Pass Number for Register Scenario3
Then Click zip code for Register Scenario3
Then Enter ZIP Code of User for Register Scenario3
Then Click  Add Swiss Pass ID button for Register Scenario3
Then Click Allow button for Register Scenario3 
Then Enter the email field for Register Scenario13
Then Choose the DOB for Register Scenario3
Then Choose the DOBYear for Register Scenario3
Then Choose the DOBMonth for Register Scenario3
Then Choose the DOBDate for Register Scenario3
Then click the Confirm Date button for Register Scenario3
Then Enter and enter the password for Register Scenario3
Then Enter and enter the confirm password for Register Scenario3
Then click the Continue button for Register Scenario3



@ALP10003 @all @sc3
Scenario: Registration Screen2 Scenario3
Then uncheck the first checkbox for Screen2 Scenario3
Then uncheck the second checkbox for Screen2 Scenario3
Then uncheck the third checkbox for Screen2 Scenario3
Then click the Next button for Register Screen2 Scenario3


@ALP20003 @all @sc3
Scenario: Registration Screen3 Scenario3
Then click third location for Register Screen3 Scenario3
Then click the Next button for Register Screen3 Scenario3


@ALP50003 @all @sc3
Scenario: Registration Screen6 Scenario3
Then click the car transport for Screen6 Scenario3
Then click the train transport for Screen6 Scenario3
Then Enter the number plate value for Register Screen6 Scenario3
Then click the Next button for Register Screen6 Scenario3


@ALP70003 @all @sc3
Scenario: Registration Screen8 Scenario3
Then Enter the Street for Register Screen8 Scenario3
Then Enter the Number for Register Screen8 Scenario3
#Then Enter the Zip code for Register Screen8 Scenario3
#Then Enter the City for Register Screen8 Scenario3
#Then Choose the Country for Register Screen8 Scenario3
Then click the Continue button for Register Screen8 Scenario3


@ALP80003 @all @sc3
Scenario: Registration Screen9 Scenario3
Then uncheck the first box for Screen9 Scenario3
Then uncheck the second box for Screen9 Scenario3
Then uncheck the third box for Screen9 Scenario3
Then uncheck the fourth box for Screen9 Scenario3
Then uncheck the fifth box for Screen9 Scenario3
Then Click the next button for Register Screen9 Scenario3



@ALP90003 @all @sc3
Scenario: Registration Screen10 Scenario3
Then Click the first checkboxes for Register Screen10 Scenario3
Then Click the second checkboxes for Register Screen10 Scenario3
Then click the Complete Sign Up button for Register Screen10 Scenario3


@ALP11003 @all @sc3
Scenario: Registration Screen11 Scenario3
Then click the Continue button2 for Register Screen11 Scenario3
Then click the Continue button for Register Screen11 Scenario3

@ALP12003 @all @sc3
Scenario: Plan a trip Screen12 Scenario3
Then Click Plan a trip button for Screen12 Scenario3

@ALP13003 @all @sc3
Scenario: Plan a trip Screen13 Scenario3
Then Select third location checkbox for Screen13 Scenario3
Then Click next button for Screen13 Scenario3

@ALP14003 @all @sc3
Scenario: Plan a trip Screen14 Scenario3
Then Click on start date button for Screen14 Scenario3
Then Select start date for Screen14 Scenario3
Then select End date for Screen14 Scenario3
Then Click start time dropdown for Screen14 Scenario3
Then select start time for Screen14 Scenario3
Then Click End time dropdown for Screen14 Scenario3
Then select end timefor Screen14 Scenario3
Then Click confirm button for Screen14 Scenario3
Then Click next button for Screen14 Scenario3

@ALP15003 @all @sc3
Scenario: Plan a trip Screen15 Scenario3
Then Select first equipment for Screen15 Scenario3
Then Select second equipment for Screen15 Scenario3
Then Select third equipment for Screen15 Scenario3
Then Select fourth equipment for Screen15 Scenario3
Then Select fifth equipment for Screen15 Scenario3
Then Select sixth equipment for Screen15 Scenario3
Then Select seventh equipment for Screen15 Scenario3
Then Select eighth equipment for Screen15 Scenario3
Then Select nineth equipment for Screen15 Scenario3
Then Click Yes button for Screen15 Scenario3

@ALP16003 @all @sc3
Scenario: Plan a trip Screen16 Scenario3
Then Click next button for Screen16 Scenario3

@ALP17003 @all @sc3
Scenario: Plan a trip Screen17 Scenario3
#Then Click uncheckbox for Screen17 Scenario3
Then Click yes button for Screen17 Scenario3

@ALP18003 @all @sc3
Scenario: Plan a trip Screen18 Scenario3
Then Click Payment button for Screen18 Scenario3


#Scenario4 Without swiss pass ID, select all locations, gender rather not say, without shoe size, snowboard skill type, Transport train, Only zipcode, without promocode, all equipment


@ALP004 @Register4 @all @sc4
Scenario: Registration Scenario4
Given open browser
Then Enter the firstname for Register Scenario4
Then Enter the lastname for Register Scenario4
Then Enter the email field for Register Scenario4
Then Choose the DOB for Register Scenario4
Then Choose the DOBYear for Register Scenario4
Then Choose the DOBMonth for Register Scenario4
Then Choose the DOBDate for Register Scenario4
Then click the Confirm Date button for Register Scenario4
Then Enter and enter the password for Register Scenario4
Then Enter and enter the confirm password for Register Scenario4
Then click the Continue button for Register Scenario4



@ALP10004 @Register4 @all @sc4
Scenario: Registration Screen2 Scenario4
Then click the Next button for Register Screen2 Scenario4


@ALP20004 @Register4 @all @sc4
Scenario: Registration Screen3 Scenario4

Then click first location for Register Screen3 Scenario4
Then click second location for Register Screen3 Scenario4
Then click third location for Register Screen3 Scenario4
Then click the Next button for Register Screen3 Scenario4


@ALP30004 @Register4 @all @sc4
Scenario: Registration Screen4 Scenario4
Then click the gender for Register Screen4 Scenario4
Then Slide the Height for Register Screen4 Scenario4
Then Slide the Weight for Register Screen4 Scenario4
Then Slide the Shoe Size for Register Screen4 Scenario4
#Then Enter the Ski boots size with decimal for Register Screen4 Scenario4
Then click the Continue button for Register Screen4 Scenario4


@ALP40004 @Register4 @all @sc4
Scenario: Registration Screen5 Scenario4
Then click the Skill type for Register Screen5 Scenario4
Then Slide the Select Skill for Register Screen5 Scenario4
Then Slide the Select Gear grade for Register Screen5 Scenario4
Then click the Continue button for Register Screen5 Scenario4


@ALP50004 @Register4 @all @sc4
Scenario: Registration Screen6 Scenario4
Then click the Transport type for Register Screen6 Scenario4
#Then Enter the number plate value for Register Screen6 Scenario4
Then click the Next button for Register Screen6 Scenario4


@ALP60004 @Register4 @all @sc4
Scenario: Registration Screen7 Scenario4
Then Slide the Accomodation for Register Screen7 Scenario4
Then click the Next button for Register Screen7 Scenario4


@ALP70004 @Register4 @all @sc4
Scenario: Registration Screen8 Scenario4
#Then Enter the Street for Register Screen8 Scenario4
#Then Enter the Number for Register Screen8 Scenario4
Then Enter the Zip code for Register Screen8 Scenario4
#Then Enter the City for Register Screen8 Scenario4
#Then Choose the Country for Register Screen8 Scenario4
Then click the Continue button for Register Screen8 Scenario4


@ALP80004 @Register4 @all @sc4
Scenario: Registration Screen9 Scenario4
Then Click the next button for Register Screen9 Scenario4
#Then Click the skip button for Register Screen9 Scenario4


@ALP90004 @Register4 @all @sc4
Scenario: Registration Screen10 Scenario4
Then Click the first checkboxes for Register Screen10 Scenario4
Then Click the second checkboxes for Register Screen10 Scenario4
Then click the Complete Sign Up button for Register Screen10 Scenario4


@ALP11004 @Register4 @all @sc4
Scenario: Registration Screen11 Scenario
#Then Click the promocode checkbox for Register Screen11 Scenario4
#Then Click promocode for Register Screen11 Scenario4
#Then Enter promocode for Register Screen11 Scenario4
Then click the Continue button for Register Screen11 Scenario4

@ALP12004  @all @sc4
Scenario: Plan a trip Screen12 Scenario4
Then Click Plan a trip button for Screen12 Scenario4

@ALP13004  @all @sc4
Scenario: Plan a trip Screen13 Scenario4
Then Select first location checkbox for Screen13 Scenario4
Then Click next button for Screen13 Scenario4

@ALP14004 @all @sc4
Scenario: Plan a trip Screen14 Scenario4
Then Click on start date button for Screen14 Scenario4
Then Select start date for Screen14 Scenario4
Then select End date for Screen14 Scenario4
Then Click start time dropdown for Screen14 Scenario4
Then select start time for Screen14 Scenario4
Then Click End time dropdown for Screen14 Scenario4
Then select end timefor Screen14 Scenario4
Then Click confirm button for Screen14 Scenario4
Then Click next button for Screen14 Scenario4

@ALP15004 @all @sc4
Scenario: Plan a trip Screen15 Scenario4
Then Select first equipment for Screen15 Scenario4
Then Select second equipment for Screen15 Scenario4
Then click on add message for Screen15 Scenario4
Then click message box for Screen15 Scenario4
Then enter message for Screen15 Scenario4
Then Click Yes button for Screen15 Scenario4

@ALP16004 @all @sc4
Scenario: Plan a trip Screen16 Scenario4
Then Click next button for Screen16 Scenario4

@ALP17004 @all @sc4
Scenario: Plan a trip Screen17 Scenario4
#Then Click checkbox for Screen17 Scenario4
Then Click yes button for Screen17 Scenario4

@ALP18004 @all @sc4
Scenario: Plan a trip Screen18 Scenario4
Then Click Payment button for Screen18 Scenario4

#Scenario 5 With menu screen automation in booking page for partial 1

@ALP005 @all @sc5
Scenario: Registration Scenario5
Given browser open
Then Enter the firstname for Register Scenario5
Then Enter the lastname for Register Scenario5
Then Enter the email field for Register Scenario5
Then Choose the DOB for Register Scenario5
Then Choose the DOBYear for Register Scenario5
Then Choose the DOBMonth for Register Scenario5
Then Choose the DOBDate for Register Scenario5
Then click the Confirm Date button for Register Scenario5
Then Enter and enter the password for Register Scenario5
Then Enter and enter the confirm password for Register Scenario5
Then click the Continue button for Register Scenario5



@ALP10005 @all @sc5
Scenario: Registration Screen2 Scenario5
Then click the Next button for Register Screen2 Scenario5


@ALP20005 @all @sc5
Scenario: Registration Screen3 Scenario5
Then click first location for Register Screen3 Scenario5
Then click the Next button for Register Screen3 Scenario5


@ALP30005 @Screen4 @all @sc5
Scenario: Registration Screen4 Scenario5
Then click the gender for Register Screen4 Scenario5
Then Slide the Height for Register Screen4 Scenario5
Then Slide the Weight for Register Screen4 Scenario5
Then Slide the Shoe Size for Register Screen4 Scenario5
Then click the Continue button for Register Screen4 Scenario5


@ALP40005 @Screen5 @all @sc5
Scenario: Registration Screen5 Scenario5
Then click the Skill type for Register Screen5 Scenario5
Then Slide the Select Skill for Register Screen5 Scenario5
Then Slide the Select Gear grade for Register Screen5 Scenario5
Then click the Continue button for Register Screen5 Scenario5


@ALP50005 @Screen6 @all @sc5
Scenario: Registration Screen6 Scenario5
Then click the Transport type for Register Screen6 Scenario5
Then Enter the number plate value for Register Screen6 Scenario5
Then click the Next button for Register Screen6 Scenario5


@ALP60005 @Screen7 @all @sc5
Scenario: Registration Screen7 Scenario5
Then Slide the Accomodation for Register Screen7 Scenario5
Then click the Next button for Register Screen7 Scenario5


@ALP70005 @Screen8 @all @sc5
Scenario: Registration Screen8 Scenario5
Then Enter the Street for Register Screen8 Scenario5
Then Enter the Number for Register Screen8 Scenario5
Then Enter the Zip code for Register Screen8 Scenario5
Then Enter the City for Register Screen8 Scenario5
Then Choose the Country for Register Screen8 Scenario5
Then click the Continue button for Register Screen8 Scenario5


@ALP80005 @Screen9 @all @sc5
Scenario: Registration Screen9 Scenario5
Then Click the next button for Register Screen9 Scenario5
#Then Click the skip button for Register Screen9 Scenario5


@ALP90005 @Screen10 @all @sc5
Scenario: Registration Screen10 Scenario5
Then Click the first checkboxes for Register Screen10 Scenario5
Then Click the second checkboxes for Register Screen10 Scenario5
Then click the Complete Sign Up button for Register Screen10 Scenario5


@ALP11005 @Screen11 @all @sc5
Scenario: Registration Screen11 Scenario5
Then Click the promocode checkbox for Register Screen11 Scenario5
Then Click promocode for Register Screen11 Scenario5
Then Enter promocode for Register Screen11 Scenario5
Then click the Continue button for Register Screen11 Scenario5

@ALPM001 @all @sc5
Scenario: Menu Scenario5
Then Click Menu button for menu screen Scenario5
Then Click My Profile for menu screen Scenario5
Then Click My Athletic Profile for menu screen Scenario5
Then Click the back arrow for menu screen Scenario5
Then Click the My Kids for menu screen Scenario5
Then Click  Add more kids button for menu screen Scenario5
Then Enter the firstname for menu screen Scenario5
Then Enter the lastname for menu screen Scenario5
Then Choose the DOB for menu screen Scenario5
Then Choose the DOBYear for menu screen Scenario5
Then Choose the DOBMonth for menu screen Scenario5
Then Choose the DOBDate for menu screen Scenario5
Then click the Confirm Date button for menu screen Scenario5
Then click the gender for menu screen Scenario5
Then Slide the Height for menu screen Scenario5
Then Slide the Weight for menu screen Scenario5
Then Slide the Shoe Size for menu screen Scenario5
Then Click Add Kids for menu screen Scenario5
Then Click Transportation Preferences for menu screen Scenario5
Then Click the back arrow transportation for menu screen Scenario5
Then Click Accommodation Preferences for menu screen Scenario5
Then Click the back arrow accommodation for menu screen Scenario5
Then Click Preferred Locations for menu screen Scenario5
Then Click the back arrow location for menu screen Scenario5
Then Click Communication Preferences for menu screen Scenario5
Then Click the back arrow communication for menu screen Scenario5
Then Click the Menu to go Home for booking for menu screen Scenario5
Then click the Home button for menu screen Scenario5

@ALP12005 @Screen12 @all @sc5
Scenario: Plan a trip Screen12 Scenario5
Then Click Plan a trip button for Screen12 Scenario5

@ALP13005 @Screen13 @all @sc5
Scenario: Plan a trip Screen13 Scenario5
Then Select first location checkbox for Screen13 Scenario5
Then Click next button for Screen13 Scenario5

@ALP14005 @Screen14 @all @sc5
Scenario: Plan a trip Screen14 Scenario5
Then Click on start date button for Screen14 Scenario5
Then Select start date for Screen14 Scenario5
Then select End date for Screen14 Scenario5
Then Click start time dropdown for Screen14 Scenario5
Then select start time for Screen14 Scenario5
Then Click End time dropdown for Screen14 Scenario5
Then select end timefor Screen14 Scenario5
Then Click confirm button for Screen14 Scenario5
Then Click next button for Screen14 Scenario5

@ALP15005 @Screen15 @all @sc5
Scenario: Plan a trip Screen15 Scenario5
Then Select first equipment for Screen15 Scenario5
#Then Select second equipment for Screen15 Scenario5
#Then Select third equipment for Screen15 Scenario5
#Then Select fourth equipment for Screen15 Scenario5
Then Click Yes button for Screen15 Scenario5

@ALP16005 @Screen16 @all @sc5
Scenario: Plan a trip Screen16 Scenario5
Then Click next button for Screen16 Scenario5

@ALP17005 @Screen17 @all @sc5
Scenario: Plan a trip Screen17 Scenario5
Then Click checkbox for Screen17 Scenario5
Then Click yes button for Screen17 Scenario5

@ALP18005 @Screen18 @all @sc5
Scenario: Plan a trip Screen18 Scenario5
Then Click Payment button for Screen18 Scenario5

@testing1 @menutest
Scenario: Menu testing Scenario1
Given open testing url
Then Click Menu button for testing
Then Click My Profile for testing
Then Click My Athletic Profile for testing
Then Click the back arrow for testing
Then Click the My Kids for testing
Then Click  Add more kids button for testing
Then Enter the firstname for testing
Then Enter the lastname for testing
Then Choose the DOB for testing
Then Choose the DOBYear for testing
Then Choose the DOBMonth for testing
Then Choose the DOBDate for testing
Then click the Confirm Date testing
Then click the gender for testing
Then Slide the Height for testing
Then Slide the Weight for testing
Then Slide the Shoe Size for testing
Then Click Add Kids for testing
Then click the back arrow add kids for testing
Then Click Transportation Preferences for testing
Then Click the back arrow transportation for testing
Then Click Accommodation Preferences for testing
Then Click the back arrow accommodation for testing
Then Click Preferred Locations for testing
Then Click the back arrow location for testing
Then Click Communication Preferences for testing
Then Click the back arrow communication for testing
Then Click the Menu to go Home for booking for testing
Then click the Home button for testing

@testing2 @menutest
Scenario: Plan a trip Screen12 testing
#Then url
Then Click Plan a trip button for testing Screen12

@testing3 @menutest
Scenario: Plan a trip Screen13 testing
Then Select first location checkbox for testing Screen13
Then Click next button for testing Screen13

@testing4 @menutest
Scenario: Plan a trip Screen14 testing
Then Click on start date button for testing Screen14
Then Select start date for testing Screen14
Then select End date for testing Screen14
Then Click start time dropdown for testing Screen14
Then select start time for testing Screen14
Then Click End time dropdown for testing Screen14
Then select end time for testing Screen14
Then Click confirm button for testing Screen14
Then Click next button for testing Screen14

@testing5 @menutest
Scenario: Plan a trip Screen15 testing
Then Select first equipment for Screen15 testing
Then Select second equipment for Screen15 testing
Then Select third equipment for Screen15 testing
Then Select fourth equipment for Screen15 testing
Then Select fifth equipment for Screen15 testing
Then Select sixth equipment for Screen15 testing
Then Select seventh equipment for Screen15 testing
Then Select eighth equipment for Screen15 testing
Then Click Yes button for Screen15 testing

@testing6 @menutest
Scenario: Plan a trip Screen16 testing
Then Click next button for Screen16 testing

@testing7 @menutest
Scenario: Plan a trip Screen17 testing
Then Click checkbox for Screen17 testing
Then Click yes button for Screen17 testing

@testing8 @menutest
Scenario: Plan a trip Screen18 testing
Then Click Payment button for Screen18 testing

#@testing9 @menutest
#Scenario: Plan a trip Screen19 testing
#Then Enter card number for Screen19 testing
#Then Enter expiry for Screen19 testing
#Then Enter ccv number for Screen19 testing
#Then Enter name on card for Screen19 testing
#Then select region for Screen19 testing
#Then click on save card checkbox for Screen19 testing
#Then Enter mobile number for Screen19 testing
#Then click on pay button for Screen19 testing
#Then click on back to home for Screen19 testing
